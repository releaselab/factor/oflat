var number = 0;

var cy = null;
var cy2 = null;
var direction = null;

var layoutDir = null;

function start () {
  number = 0;
  cy = window.cy = cytoscape({
    container: document.getElementById('cy'),
    layout: {
      name: 'grid',
      rows: 2,
      cols: 2
    },
    style: [
      {
        selector: 'node[name]',
        style: {
          'content': 'data(name)',
          'width': '40px',
          'height': '40px',
          'text-valign': 'bottom',
          'text-halign': 'center'
        }
      },
      {
        selector: 'edge[symbol]',
        style: {
          'content': 'data(symbol)'
        }
      },
      {
        selector: 'edge',
        style: {
          'curve-style': 'bezier',
          'target-arrow-shape': 'triangle'
        }
      },
    {
      selector: '#transparent',
      style: {
        'visibility': 'hidden'
      }
    },
    {
      selector: '.SUCCESS',
      style: {
        'border-width': '7px',
        'border-color': 'black',
        'border-style': 'double'
      }
  },
    ],
    elements: {
      nodes: [
        {data: { id: 'transparent', name: 'transparent' }}
      ]
    }
  });
  cy.autounselectify( false );
  cy.$('#transparent').position('y', 200);
  cy.$('#transparent').position('x', -200);
  cy.$('#transparent').lock();
}

var stack = new Array ();
var conjunto = [[], []];
var counter = 0;
var number = 0;

function makeNewTree (s) {
  var index = s.indexOf("|");
  var index2 = s.indexOf ("/");
  var st = "|";
  if (index > index2) {
    index = index2;
    st = "/";
  }
  var str = "";
  var newString = "";
  if (index != -1) {
    str = (s.substring(0, index));
    newString = (s.substring(index + 1, s.length));
  } 
  if (st == "/") {
    stack.pop();
  } else {
    var newId = "n" + number;
    if (str.length != 0) {
    if (stack.length != 0) {
      conjunto[1].push ({data: {source: stack[stack.length-1], target: newId}});
    }
      conjunto[0].push ({data: {id: newId, name: str}, classes: str});
      stack.push (newId);
      number++;
    }
  }
  if (newString.length != 0) {
    makeNewTree (newString);
  }
}

function makeTree1 (s) {
  var idgen = "n" + number;
  number ++;
  var str = s;
  var st = str[0];
  switch (st) {
    case 'E': return [idgen, [{data: {id: idgen, name: "()"}}], [], str.substr(1)];
    case '+': var [lid, lnode, ledge, lret] = makeTree1 (str.substr(1));
              var [rid, rnode, redge, rret] = makeTree1 (lret);
              return [idgen, [{data: {id: idgen, name: "+" }}].concat(lnode).concat(rnode), [{data: {source: idgen, target: lid}}].concat([{data: {source: idgen, target: rid}}]).concat(ledge).concat(redge), rret];
    case '*': var [cid, cnode, cedge, cret] = makeTree1 (str.substr(1));
              return [idgen, [{data: {id: idgen, name: "*"}}].concat(cnode), [{data: {source: idgen, target: cid}}].concat(cedge), cret];
    case '.': var [lid, lnode, ledge, lret] = makeTree1 (str.substr(1));
              var [rid, rnode, redge, rret] = makeTree1 (lret);
              return [idgen, [{data: {id: idgen, name: "."}}].concat(lnode).concat(rnode), [{data: {source: idgen, target: lid}}].concat([{data: {source: idgen, target: rid}}]).concat(ledge).concat(redge), rret];
    default: return [idgen, [{data: {id: idgen, name: st}}], [], str.substr(1)];
  }
}

function startTree(nString) {
  let teste = makeTree1 (nString);
  layoutDir = "LR";
  cy = window.cy = cytoscape({
    container: document.getElementById('cy'),
    boxSelectionEnabled: false,
    autounselectify: true,
    layout: {
      name: 'dagre',
      rankDir: 'LR'
    },
    style: [
      {
        selector: 'node[name]',
        style: {
          'content': 'data(name)',
          'width': '40px',
          'height': '40px',
          'text-valign': 'center',
          'text-halign': 'center',
          'font-size': '20px'
        }
      },
      {
        selector: 'node',
        style: {
          'background-color': 'white'
        }
      },
      {
        selector: 'edge',
        style: {
          'width': 4,
          'target-arrow-shape': 'triangle',
          'line-color': '#9dbaea',
          'target-arrow-color': '#9dbaea',
          'curve-style': 'bezier'
        }
      }
    ],
    elements: {
      nodes: teste[1],
      edges: teste[2]
    }
  });
  direction = 'LR';

};

function startTree1(nString) {
  stack = new Array ();
  conjunto = [[], []];
  counter = 0;
  number = 0;
  makeNewTree (nString);
  cy2 = window.cy2 = cytoscape({
    container: document.getElementById('cy2'),
    boxSelectionEnabled: false,
    autounselectify: true,
    layout: {
      name: 'dagre',
      rankDir: 'TB'
    },
    style: [
      {
        selector: 'node[name]',
        style: {
          'content': 'data(name)',
          'width': '110px',
          'height': '40px',
          'text-valign': 'center',
          'text-halign': 'center',
          'font-size': '20px'
        }
      },
      {
        selector: '.Fail',
        style: {
          'color': 'red'
        }
      },
      {
        selector: 'node',
        style: {
          'background-color': 'white'
        }
      },
      {
        selector: 'edge',
        style: {
          'width': 4,
          'target-arrow-shape': 'triangle',
          'line-color': '#9dbaea',
          'target-arrow-color': '#9dbaea',
          'curve-style': 'bezier'
        }
      }
    ],
    elements: {
      nodes: conjunto[0],
      edges: conjunto[1]
    }
  });

};

function changeToHorizontal () {
  cy.layout ({name: 'dagre', rankDir: 'LR'}).run()
};

function changeToVertical () {
  cy.layout ({name: 'dagre', rankDir: 'TB'}).run()
};

function changeDirection () {
  if (layoutDir == "LR") {

    layoutDir = "TB";
    cy.layout ({name: 'dagre', rankDir: 'TB'}).run();
    
  } else {
    layoutDir = "LR";
    cy.layout ({name: 'dagre', rankDir: 'LR'}).run();
  }
}


function makeNode (nm, isStart, final)  {
  var  verify = cy.getElementById (nm);
  if (verify.length < 1) { 
    if (final == "true") {
    if (isStart == "true") {
      cy.add({
        data: { id: nm, name: nm }, classes: 'SUCCESS'
      });
      cy.$('#' + nm).position('y', 200);
      cy.$('#' + nm).position('x', -100);
      cy.$('#' + nm).lock();
      makeEdge ('transparent', nm, '')
    } else {
        cy.add({
          data: { id: nm, name: nm },
          position: { x: Math.floor(Math.random() * 1399), y: Math.floor(Math.random() * 299) }, classes: 'SUCCESS'
        });
    }
    } else {
      if (isStart == "true") {
        cy.add({
          data: { id: nm, name: nm }
        });
        cy.$('#' + nm).position('y', 200);
        cy.$('#' + nm).position('x', -100);
        cy.$('#' + nm).lock();
        makeEdge ('transparent', nm, '')
      } else {
          cy.add({
            data: { id: nm, name: nm },
            position: { x: Math.floor(Math.random() * 1399), y: Math.floor(Math.random() * 299) }
          });
      }
    }
    cy.fit();
  }
};

function makeEdge (first, second, third)  {

  var nId = first + second;
  var getEdge = cy.getElementById(nId);

  if (getEdge.length  == 0) {
    cy.add({
      data: { id: nId, source: first, symbol: third, target: second }
    });
  } else {
    var k = getEdge.data('symbol');
    getEdge.remove();
    var newsymbol = k + ', ' + third;
    cy.add({
      data: { id: nId, source: first, symbol: newsymbol, target: second }
    })
  }
};

function removeNode (node) {
  var getNode = cy.getElementById(node);
  getNode.remove();
}

function removeEdge (first, second, third)  {

  var nId = first + second;
  var getEdge = cy.getElementById(nId);

  var k = getEdge.data('symbol');

  var g = "";

  for (i = 0; i < k.length; i++) {
    var j = k.charAt (i);
    if (j != ',' && j != ' ' && j != third) {
      if (g.length == 0) {
        g = j;
      } else {
        g = g + ', ' + j;
      }
    }
  }
  getEdge.remove();
  if (g.length > 0) {
    cy.add({
      data: { id: nId, source: first, symbol: g, target: second}
    })
  }
};

function destroy1 () {
  if (cy != null) {
    cy.destroy();
    cy = null;
  }

};

function fit () {
  if (cy != null) {
    cy.resize();
    cy.fit();
  }

};

function resetStyle () {
  cy.style()
    .resetToDefault()
    .selector ('node[name]')
    .style ({'content': 'data(name)',
          'width': '40px',
          'height': '40px', 'text-valign': 'bottom',
          'text-halign': 'center'})
    .selector( 'edge[symbol]')
    .style ( {
                'content': 'data(symbol)'
              })
    .selector( 'edge')
    .style ({
            'curve-style': 'bezier',
            'target-arrow-shape': 'triangle'
          })
          .selector ('.SUCCESS')
          .style ({
                'border-width': '10px',
                'border-color': 'black',
                'border-style': 'double'
              })
            .selector( '#transparent')
            .style ({
                    'visibility': 'hidden'
                  })
    .update()
}

function paintNode (node, color) {
  cy.style ()
    .selector('#' + node)
    .style ( {
      'background-color': color
    })
    .update()
}

function myalert(s) { alert("||| " + s + " |||") }

function fileSelectAction() {
  alert ("Teste 1");
	const file = window.event.target.files[0];
	if( file == undefined ) // if canceled
		return;
  const reader = new FileReader();
  window.fileContents = "";
	reader.onload = function(event) { window.fileContents = event.target.result; };
  reader.readAsText(file);

  
}

function start2 () {
  cy2 = window.cy2 = cytoscape({
    container: document.getElementById('cy2'),
    layout: {
      name: 'grid',
      rows: 2,
      cols: 2
    },
    style: [
      {
        selector: 'node[name]',
        style: {
          'content': 'data(name)',
          'width': '40px',
          'height': '40px',
          'text-valign': 'bottom',
          'text-halign': 'center'
        }
      },
      {
        selector: 'edge[symbol]',
        style: {
          'content': 'data(symbol)'
        }
      },
      {
        selector: 'edge',
        style: {
          'curve-style': 'bezier',
          'target-arrow-shape': 'triangle'
        }
      },
    {
      selector: '#transparent1',
      style: {
        'visibility': 'hidden'
      }
    },
    {
      selector: '.SUCCESS',
      style: {
        'border-width': '7px',
        'border-color': 'black',
        'border-style': 'double'
      }
  },
    ],
    elements: {
      nodes: [
        {data: { id: 'transparent1', name: 'transparent1' }}
      ]
    }
  });
  cy2.$('#transparent1').position('y', 200);
  cy2.$('#transparent1').position('x', -200);
  cy2.$('#transparent1').lock();
}

function makeNode2 (nm, isStart, final)  {
  var  verify = cy2.getElementById (nm);
  if (verify.length < 1) { 
    if (final == "true") {
    if (isStart == "true") {
      cy2.add({
        data: { id: nm, name: nm }, classes: 'SUCCESS'
      });
      cy2.$('#' + nm).position('y', 200);
      cy2.$('#' + nm).position('x', -100);
      cy2.$('#' + nm).lock();
      makeEdge2 ('transparent1', nm, '')
    } else {
        cy2.add({
          data: { id: nm, name: nm },
          position: { x: Math.floor(Math.random() * 1399), y: Math.floor(Math.random() * 299) }, classes: 'SUCCESS'
        });
    }
    } else {
      if (isStart == "true") {
        cy2.add({
          data: { id: nm, name: nm }
        });
        cy2.$('#' + nm).position('y', 200);
        cy2.$('#' + nm).position('x', -100);
        cy2.$('#' + nm).lock();
        makeEdge2 ('transparent1', nm, '')
      } else {
          cy2.add({
            data: { id: nm, name: nm },
            position: { x: Math.floor(Math.random() * 1399), y: Math.floor(Math.random() * 299) }
          });
      }
    }
    cy2.fit();
  }
};

function makeEdge2 (first, second, third)  {

  var nId = first + second;
  var getEdge = cy2.getElementById(nId);

  if (getEdge.length  == 0) {
    cy2.add({
      data: { id: nId, source: first, symbol: third, target: second }
    });
  } else {
    var k = getEdge.data('symbol');
    getEdge.remove();
    var newsymbol = k + ', ' + third;
    cy2.add({
      data: { id: nId, source: first, symbol: newsymbol, target: second }
    })
  }

  cy2.fit();
};

function destroy2 () {
  if (cy2 != null) {
    cy2.destroy();
  }
  cy2 = null;

}; 

function paintNode1 (node, color) {
  cy2.style ()
    .selector('#' + node)
    .style ( {
      'background-color': color
    })
    .update()
}

function makeTreeNode2 (nm)  {
  var newId = 'n' + number;
    ids.push (newId);
    cy2.add({
      data: { id: newId, name: nm }
    });

  cy.fit();
};

function makeTreeEdge2 (first, second)  {

    cy2.add({
      data: { source: first, target: second }
    });
  cy2.fit();
};



