(** OFLAT - A graphical interface for the OCaml-Flat library written using the Ocsigen framework
 * 
 * @version v 0.2
 * 
 * @author Rita Macedo <rp.macedo@campus.fct.unl.pt>  [main author]
 * @author Joao Goncalves <jmg.goncalves@campus.fct.unl.pt>
 * @author Artur Miguel Dias <amd@fct.unl.pt>
 * @author Antonio Ravara <aravara@fct.unl.pt>
 * 
 * LICENCE - As now this is a private project, but later it will be open source.
 *  
*)


[%%shared
    open Eliom_lib
    open Eliom_content
    open Html.D
    open OCamlFlatSupport
    open OCamlFlat
    open Js_of_ocaml
    open CSS
    open OflatGraphics
]

module OFlat_app =
  Eliom_registration.App (
    struct
      let application_name = "oflat"
      let global_data_path = None
    end)

let main_service =
  Eliom_service.create
    ~path:(Eliom_service.Path [])
    ~meth:(Eliom_service.Get Eliom_parameter.unit)
    ()

(** --- Scripts called with the creation of the html page --- **)
let script_uri =
  Eliom_content.Html.D.make_uri
      ~absolute:false   (* We want local file *)
      ~service:(Eliom_service.static_dir ())
      ["graphLibrary.js"]


let script_uri1 =
  Eliom_content.Html.D.make_uri
  (Eliom_service.extern
     ~prefix:"https://unpkg.com"
     ~path:["cytoscape";"dist"]
     ~meth:
       (Eliom_service.Get
          Eliom_parameter.(suffix (all_suffix "suff")))
     ())
    ["cytoscape.min.js"]

let script_uri5 =
  Eliom_content.Html.D.make_uri
  (Eliom_service.extern
     ~prefix:"https://cdnjs.cloudflare.com"
     ~path:["ajax"; "libs"; "lodash.js"; "4.17.10"]
     ~meth:
       (Eliom_service.Get
          Eliom_parameter.(suffix (all_suffix "suff")))
     ())
    ["lodash.js"]

  let script_uri7 =
  Eliom_content.Html.D.make_uri
  (Eliom_service.extern
     ~prefix:"https://cytoscape.org"
     ~path:["cytoscape.js-edgehandles"]
     ~meth:
       (Eliom_service.Get
          Eliom_parameter.(suffix (all_suffix "suff")))
     ())
    ["cytoscape-edgehandles.js"]

    let script_uri8 =
  Eliom_content.Html.D.make_uri
  (Eliom_service.extern
     ~prefix:"https://unpkg.com"
     ~path:["dagre@0.7.4"; "dist"]
     ~meth:
       (Eliom_service.Get
          Eliom_parameter.(suffix (all_suffix "suff")))
     ())
    ["dagre.js"]

  let script_uri9 =
  Eliom_content.Html.D.make_uri
  (Eliom_service.extern
     ~prefix:"https://cytoscape.org"
     ~path:["cytoscape.js-dagre"]
     ~meth:
       (Eliom_service.Get
          Eliom_parameter.(suffix (all_suffix "suff")))
     ())
    ["cytoscape-dagre.js"]

  (** links use on the page footer **)
  let lincs_service =
  Eliom_content.Html.D.a
  (Eliom_service.extern
     ~prefix:"http://nova-lincs.di.fct.unl.pt"
     ~path:[""]
     ~meth:
       (Eliom_service.Get
          Eliom_parameter.(suffix (all_suffix "suff")))
     ())
    [div ~a:[a_id "footerButton"][txt "NOVA-LINCS"]]
    [""]

  let factor_service =
  Eliom_content.Html.D.a
  (Eliom_service.extern
     ~prefix:"https://release.di.ubi.pt"
     ~path:["factor"]
     ~meth:
       (Eliom_service.Get
          Eliom_parameter.(suffix (all_suffix "suff")))
     ())
    [div ~a:[a_id "footerButton"][txt "Factor"]]
    ["index.html"]

  let tezos_service =
  Eliom_content.Html.D.a
  (Eliom_service.extern
     ~prefix:"https://tezos.com/"
     ~path:[""]
     ~meth:
       (Eliom_service.Get
          Eliom_parameter.(suffix (all_suffix "suff")))
     ())
    [div ~a:[a_id "footerButton"][txt "Fundação Tezos"]]
    [""]

 (** ------------------------------------------------------------------------
 * --- Calling the server --------------------------------------------------
 * -------------------------------------------------------------------------
 *
 *   The functions in the client part of the module Server allows the
 *   client to access certain server-side operations.
 *)

module%server Server =
  struct
    let examplesDirPath =
        Sys.getcwd () ^ "/static/examples/"

    let log (str: string) =
        print_string (str^"\n");
        flush stdout;
        Lwt.return ()

    let getExamplesList () =
        let fileList = 
            try
                Array.to_list (Sys.readdir examplesDirPath)
            with _ -> [] in
        Lwt.return fileList

    let getExample fname =
        Lwt.return (Util.load_file (examplesDirPath ^ fname))
  end

module%client Server =
  struct
    let log = (* logs a message in the server console *)
      ~%(Eliom_client.server_function [%json: string] Server.log)

    let getExamplesList =
      ~%(Eliom_client.server_function [%json: unit] Server.getExamplesList)

    let getExample =
      ~%(Eliom_client.server_function [%json: string] Server.getExample)
  end
(* --------------------------------------------------------------------------
 * -------------------------------------------------------------------------- *)


[%%client
module StateVariables = 
  struct
    (** -------------------------------  State Variables --------------------------------------- **)
    (** Can be "finite automaton", "regular expression" or "clean" **)
    let cyType = ref "clean"

    (** Can be "finite automaton", "regular expression", "enumeration", "info", "verify" or "clean" **)
    let cy2Type = ref "clean"

    let cySize = ref 100

    let automata = ref (new FiniteAutomatonAnimation.model (Representation {
      alphabet = Set.empty;
	    allStates = Set.make ["NotInitial"]; 
      initialState = "NotInitial";
      transitions = Set.empty;
      acceptStates = Set.empty
    }))

    let automata1 = ref (new FiniteAutomatonAnimation.model (Representation {
      alphabet = Set.empty;
	    allStates = Set.make ["STARTZ"]; 
      initialState = "STARTZ";
      transitions = Set.empty;
      acceptStates = Set.empty
    }))

    let re = ref (new RegularExpressionAnimation.model (Representation Empty))

    let enum = ref (new Enumeration.enum (Representation {
      problem = "No exercise yet";
      inside = Set.empty;
      outside = Set.empty;
    }))

    let resultTree = ref false

    (** -------------------------------  Functions --------------------------------------- **)
    let changeAutomata res =
      automata := res 

    let returnAutomata () =
      !automata

    let returnAutomataRE () =
      automata

    let changeAutomata1 res =
      automata1 := res 

    let returnAutomata1 () =
      !automata1

    let changeRe res =
      re := res 

    let returnRe () =
      !re

    let returnReNR () =
      re

    let changeEnum res =
      enum := res 

    let returnEnum () =
      !enum

    let changeResultTree res =
      resultTree := res 

    let returnResultTree () =
      !resultTree

    let changeCy1ToAutomaton () =
      cyType := "finite automaton"

    let changeCy1ToRegex () =
      cyType := "regular expression"

    let changeCy1ToText () =
      cyType := "info"

    let cleanCy1Type () =
      cyType := "clean"

    let getCy1Type() =
      !cyType

    let getAutomatonType() = "finite automaton"

    let getRegexType() = "regular expression"

    let getEnumerationType() = "enumeration"

    let getInfoType() = "info"

    let getVerifyType() = "verify"

    let getClean() = "clean"

    let changeCy2ToAutomaton () =
      cy2Type := "finite automaton"

    let changeCy2ToRegex () =
      cy2Type := "regular expression"

    let changeCy2ToEnumeration () =
      cy2Type := "enumeration"

    let changeCy2ToInfo () =
      cy2Type := "info"
    
    let changeCy2ToVerify () =
      cy2Type := "verify"

    let cleanCy2Type () =
      cy2Type := "clean"

    let getCy2Type() =
      !cy2Type

    let completeSize() =
      cySize := 100
    
    let halfSize() =
      cySize := 45

  end

module rec Controller: sig 

  val createText: Js_of_ocaml.Js.js_string Js_of_ocaml.Js.t -> unit
  val printErrors: unit -> unit
  val closeRight: unit -> unit

  val change: unit -> unit

  val eliminateNode: OCamlFlat.state -> unit 
  val createTransition: OCamlFlat.state * OCamlFlat.symbol * OCamlFlat.state -> unit
  val eliminateTransition: OCamlFlat.state * OCamlFlat.symbol * OCamlFlat.state -> unit
  
  val paintAllProductives: unit -> unit
  val paintAllReachable: unit -> unit
  val paintAllUseful: unit -> unit

  val getNewSentence: unit -> Js_of_ocaml.Js.js_string Js_of_ocaml.Js.t

  val accept: string -> unit
  val startStep: string -> unit
  val nextStep: unit -> unit
  val backStep: unit -> unit

  val defineMinimized: unit -> unit
  val getDeterministic: unit -> unit
  val cleanUseless: unit -> unit

  val automatonToRegExp: unit -> unit

  val closeCompleteAutomaton : unit -> unit
  val getCompleteAutomaton: unit -> unit

  val getWords: int -> unit

  val defineRegularExpression: RegularExpressionAnimation.model -> unit
  val defineRegularExpression1: < representation : OCamlFlatSupport.RegExpSyntax.t; .. > -> unit

  val acceptRegularExpression: string -> unit
  val fromExpressionToAutomaton: unit -> unit

  val check: unit -> unit 
  val clearEnum: unit -> unit 

  val previousTree: unit -> unit
  val nextTree: unit -> unit

  val addInitialNode: OCamlFlat.state -> unit
  val addFinalNode: OCamlFlat.state -> unit
  val addNode: OCamlFlat.state -> unit

  val about: unit -> unit 
  val feedback: unit -> unit
end 
=

  struct

    let listColors = [|"Red"; "Yellow"; "Cyan"; "Green"; "Indigo"; "Blue"; "Magenta"; "Sienna"; "Violet"; "Orange"; "Lime"; "Teal"; "SteelBlue"; "Silver"; "Olive"; "Salmon"; "Crimson"; "Purple"; "DarkKhaki"; "PowderBlue"|]
    let listColorsBig: string array ref = ref [||]

    let defineInformationBoxAutomaton () =
      let automaton = StateVariables.returnAutomata() in 
      HtmlPageClient.defineInformationBoxAutomaton();
      let deter = automaton#isDeterministic in 
        HtmlPageClient.getDeterminim deter;
      let min = automaton#isMinimized in 
        HtmlPageClient.getMinimism min;
      let useful = automaton#areAllStatesUseful in
      let uStates = automaton#getUselessStates in 
        HtmlPageClient.getHasUselessStates useful uStates;
      let nStates = automaton#numberStates in 
        HtmlPageClient.getNumberStates nStates;
      let nTransitions = automaton#numberTransitions in
        HtmlPageClient.getNumberTransitions nTransitions

    let defineExample example =
      if StateVariables.getCy2Type() <> StateVariables.getEnumerationType() then
        (HtmlPageClient.oneBox());
      HtmlPageClient.clearBox1();
      StateVariables.changeCy1ToAutomaton();
      HtmlPageClient.disableButtons (StateVariables.getCy1Type());
      Graphics.destroyGraph();
      HtmlPageClient.putCyAutomataButtons ();
      Graphics.startGraph();
      StateVariables.changeAutomata example;
      (StateVariables.returnAutomata())#drawExample;
      defineInformationBoxAutomaton ()

    let rec func (re: RegExpSyntax.t) = 
      match re with
        | Plus (l, r) -> "+" ^ func l ^ func r
        | Seq (l, r) -> "." ^ func l ^ func r
        | Star (re) -> "*" ^ func re
        | Symb (b) -> String.make 1 b
        | Empty -> "E"
        | Zero -> "Z"

    let defineRegularExpression example =
      if StateVariables.getCy2Type() <> StateVariables.getEnumerationType() then
        (HtmlPageClient.oneBox());
      HtmlPageClient.clearBox1();
      StateVariables.changeCy1ToRegex ();
      HtmlPageClient.disableButtons (StateVariables.getCy1Type());
      StateVariables.changeRe example;
      Graphics.destroyGraph();
      HtmlPageClient.putCyREButtons();
      let test = RegExpSyntax.toString (StateVariables.returnRe())#representation in
      HtmlPageClient.defineRE test;
      let test1 = func ((StateVariables.returnRe())#representation) in 
      Graphics.startGraph2(test1)

    let defineRegularExpression1 example =
      HtmlPageClient.twoBoxes ();
      HtmlPageClient.clearBox2 ();
      StateVariables.changeCy2ToRegex ();
      HtmlPageClient.putCy2Buttons();
      let text = RegExpSyntax.toString example#representation in
      HtmlPageClient.putCy2Text text
      
    let setColor number =
      
      if (number <= 20) then 
        listColorsBig := listColors
      else 
        (for i=0 to 19 do 
          Array.set !listColorsBig i (Array.get listColors i)
        done;
        for i=20 to number-1 do
          let newColor = Graphics.getRandom() in 
          Array.set !listColorsBig i newColor
        done) 

    let defineMinimized () =
      if ((StateVariables.returnAutomata())#isDeterministic) then
        if ((StateVariables.returnAutomata())#isMinimized) then 
          JS.alert ("O Autómato já é minimo")
        else 
          (HtmlPageClient.twoBoxes();
          StateVariables.changeCy2ToAutomaton();
          HtmlPageClient.clearBox2();
          HtmlPageClient.putCy2Buttons ();
          let number = (StateVariables.returnAutomata())#getColors in
          setColor number;
          Graphics.startGraph1();
          Graphics.fit();
          (StateVariables.returnAutomata())#paintMinimization !listColorsBig;
          StateVariables.changeAutomata1 ((StateVariables.returnAutomata())#minimize1);
          (StateVariables.returnAutomata1())#drawMinimize !listColorsBig number)
      else 
        JS.alert ("O Autómato tem de ser determinista para poder ser minimizado")

    let getCompleteAutomaton () =
        HtmlPageClient.twoBoxes ();
        HtmlPageClient.clearBox2();
        StateVariables.changeCy2ToInfo ();
        let buttonBox = Dom_html.getElementById "buttonBox1" in
        let test = Eliom_content.Html.To_dom.of_div (div [HtmlPageClient.closeInfo()]) in 
            Js_of_ocaml.Dom.appendChild buttonBox test;
        let getInfo = JSon.to_string ((StateVariables.returnAutomata())#toJSon) in
        let textBox = Dom_html.getElementById "textBox" in
          let test = Eliom_content.Html.To_dom.of_div (div ~a: [a_id "infoAutomata"] [pre [txt getInfo]]) in 
            Js_of_ocaml.Dom.appendChild textBox test;
        Graphics.fit()

    let eliminateNodeTransitions (a, b, c) node = 
      if (a = node || c = node) then
        (StateVariables.changeAutomata ((StateVariables.returnAutomata())#eliminateTransition (a, b, c)); 
          defineInformationBoxAutomaton ())

    let eliminateNode node =
      if (node = (StateVariables.returnAutomata())#representation.initialState) then
        JS.alert ("Não é possível eliminar estado inicial, troque o estado inicial para outro e depois elimine o indicado!") 
      else 
        if (Set.belongs node (StateVariables.returnAutomata())#representation.allStates) then 
          (let isFinal = Set.belongs node (StateVariables.returnAutomata())#representation.acceptStates in 
          StateVariables.changeAutomata ((StateVariables.returnAutomata())#eliminateNode node false isFinal);
          Set.iter (fun el -> (eliminateNodeTransitions el node)) (StateVariables.returnAutomata())#representation.transitions;
          Graphics.eliminateNode node;
          defineInformationBoxAutomaton ())
        else 
           JS.alert ("O estado indicado não existe!") 
  
    let createTransition (v1, c3, v2) =
      if (StateVariables.getCy1Type() <> StateVariables.getAutomatonType()) then
         JS.alert ("É necessário criar primeiro um estado inicial")
      else
      if (Set.belongs (v1, c3, v2) (StateVariables.returnAutomata())#representation.transitions) then
        JS.alert ("A transição (" ^ v1 ^ ", " ^ String.make 1 c3 ^ ", " ^ v2 ^ ") já existe!") 
      else 
      (if (Set.belongs v1 (StateVariables.returnAutomata())#representation.allStates) != true then
            JS.alert ("O estado de partida não existe!")
          else 
            if (Set.belongs v2 (StateVariables.returnAutomata())#representation.allStates) != true then
              JS.alert ("O estado de chegada não existe!")
            else 
              ((ignore (StateVariables.changeAutomata ((StateVariables.returnAutomata())#newTransition (v1, c3, v2))));
              Graphics.createEdge (v1, c3, v2);
              defineInformationBoxAutomaton ()))

    let eliminateTransition (v1, c3, v2) =
      if (Set.belongs (v1, c3, v2) (StateVariables.returnAutomata())#representation.transitions) then
        (StateVariables.changeAutomata ((StateVariables.returnAutomata())#eliminateTransition(v1, c3, v2));
        Graphics.eliminateEdge (v1, c3, v2);
        defineInformationBoxAutomaton ())
      else 
        JS.alert ("A transição (" ^ v1 ^ ", " ^ String.make 1 c3 ^ ", " ^ v2 ^ ") não existe!") 

    let paintAllProductives () =
      Graphics.resetStyle();
      (StateVariables.returnAutomata())#productivePainting
    
    let paintAllReachable () = 
      Graphics.resetStyle();
      (StateVariables.returnAutomata())#reachablePainting

    let paintAllUseful () =
      Graphics.resetStyle();
      (StateVariables.returnAutomata())#usefulPainting

    let acceptAutomaton word = 
        let w = (StateVariables.returnAutomata())#stringAsList1 word in
        ignore ((StateVariables.returnAutomata())#accept3 w)

    let startStep word =
      (StateVariables.returnAutomata())#changeTheTestingSentence word;
      ignore ((StateVariables.returnAutomata())#startAccept)

    let nextStep () =
      ignore ((StateVariables.returnAutomata())#next)

    let backStep () = 
      ignore ((StateVariables.returnAutomata())#back)

    let getNewSentence () = 
      Js_of_ocaml.Js.string (StateVariables.returnAutomata())#newSentence1

    let change () = 
      if (StateVariables.getCy2Type() = StateVariables.getAutomatonType()) then
        (HtmlPageClient.oneBox();
        HtmlPageClient.clearBox1();
        StateVariables.changeCy1ToAutomaton ();
        HtmlPageClient.disableButtons (StateVariables.getCy1Type());
        Graphics.destroyGraph();
        HtmlPageClient.putCyAutomataButtons ();
        Graphics.startGraph();
        StateVariables.changeAutomata (StateVariables.returnAutomata1());
        (StateVariables.returnAutomata())#drawExample;
        defineInformationBoxAutomaton();
        HtmlPageClient.clearBox2();
        let rexp = Dom_html.getElementById "regExp" in
          rexp##.innerHTML := Js_of_ocaml.Js.string "";
        StateVariables.cleanCy2Type ())
      else 
        (HtmlPageClient.oneBox ();
        let cy = Dom_html.getElementById "buttonBox" in
          cy##.innerHTML := Js_of_ocaml.Js.string "";
        let rexp = Dom_html.getElementById "regExp" in
          rexp##.innerHTML := Js_of_ocaml.Js.string "";
        let infoBox = Dom_html.getElementById "infoBox" in
          infoBox##.innerHTML := Js_of_ocaml.Js.string "";
        Graphics.destroyGraph ();
        StateVariables.cleanCy1Type ();
        StateVariables.cleanCy2Type ())

    let getDeterministic () =
      if ((StateVariables.returnAutomata())#isDeterministic) then 
        JS.alert ("O Autómato já é determinista")
      else 
        (HtmlPageClient.twoBoxes ();
        Graphics.destroyGraph1();
        StateVariables.changeCy2ToAutomaton ();
        let buttonBox = Dom_html.getElementById "buttonBox1" in
        let test1 = Eliom_content.Html.To_dom.of_div (div [HtmlPageClient.closeRight()]) in 
        Js_of_ocaml.Dom.appendChild buttonBox test1;
        Graphics.startGraph1();
        Graphics.fit();
        StateVariables.changeAutomata1 ((StateVariables.returnAutomata())#toDeterministic1);
        (StateVariables.returnAutomata1())#drawExample1)
    
    let checkHelper isWhat = 
        let result = isWhat#checkEnumeration (StateVariables.returnEnum()) in 
        HtmlPageClient.defineResult result;
        let (insideErrors, outsideErrors) = isWhat#checkEnumerationFailures (StateVariables.returnEnum()) in 
        Set.iter (fun el -> if Set.belongs el insideErrors then HtmlPageClient.createSpanList el "error" "inside" 
                              else HtmlPageClient.createSpanList el "right" "inside") (StateVariables.returnEnum())#representation.inside;
        Set.iter (fun el -> if Set.belongs el outsideErrors then HtmlPageClient.createSpanList el "error" "outside" 
                              else HtmlPageClient.createSpanList el "right" "outside") (StateVariables.returnEnum())#representation.outside

    let check () =
      if StateVariables.getCy1Type() = StateVariables.getRegexType() && StateVariables.getCy2Type() = StateVariables.getEnumerationType() then
        checkHelper (StateVariables.returnRe())
      else
        checkHelper (StateVariables.returnAutomata())

    let defineEnum e =
      HtmlPageClient.twoBoxes ();
      HtmlPageClient.clearBox2();
      StateVariables.changeEnum e;
      StateVariables.changeCy2ToEnumeration ();
      HtmlPageClient.putEnumButton ();
      HtmlPageClient.addEnumTitle();
      let prob = (StateVariables.returnEnum())#representation.problem in
        HtmlPageClient.defineEnumProblem prob;
      HtmlPageClient.addAcceptedTitle ();
      Set.iter (fun el -> HtmlPageClient.createSpanList el "nothing" "inside") (StateVariables.returnEnum())#representation.inside;
      HtmlPageClient.addNonAcceptTitle ();
      Set.iter (fun el -> HtmlPageClient.createSpanList el "nothing" "outside") (StateVariables.returnEnum())#representation.outside;
      HtmlPageClient.addEnumCheckButton ()

    let createText texto = 
      let txt = Js_of_ocaml.Js.to_string texto in
      let j = JSon.from_string txt in
			let kind = JSon.field_string j "kind" in
				if FiniteAutomaton.modelDesignation() = kind then
					(let fa = new FiniteAutomatonAnimation.model (JSon j) in 
          defineExample fa)
				else
          if RegularExpressionAnimation.modelDesignation() = kind then
            (let re = new RegularExpressionAnimation.model (JSon j) in 
            defineRegularExpression re)
          else 
					  ( let enu = new Enumeration.enum (JSon j) in 
            defineEnum enu)

    let getWords v = 
      StateVariables.changeCy2ToInfo ();
      HtmlPageClient.twoBoxes();
      HtmlPageClient.putCy2Buttons ();
      let var =
      if (StateVariables.getCy1Type() = StateVariables.getAutomatonType()) then
        (StateVariables.returnAutomata())#generateUntil v
      else 
        (StateVariables.returnRe())#generate v in 
      HtmlPageClient.putWords var
      

    let cleanUseless () =
      if ((StateVariables.returnAutomata())#areAllStatesUseful) then 
        JS.alert ("O Autómato não tem estados para limpar, não existem estados inúteis!")
      else 
        (HtmlPageClient.twoBoxes();
        HtmlPageClient.putCy2Buttons ();
        StateVariables.changeCy2ToAutomaton ();
        Graphics.startGraph1();
        StateVariables.changeAutomata1 ((StateVariables.returnAutomata())#cleanUselessStates1);
        (StateVariables.returnAutomata1())#drawExample1;
        defineInformationBoxAutomaton ())

    let closeCompleteAutomaton() =
      HtmlPageClient.oneBox ();
      StateVariables.cleanCy2Type ();
      Graphics.fit()

    let printErrors () =
      let errors = (StateVariables.returnAutomata())#errors in
      if errors = [] then 
      ()
      else 
        JS.alert (String.concat "\n" errors)

    let wordAsList = ref []
    
    let resultCount resultBool = 
      let textBox = Dom_html.getElementById "textBox" in
      if resultBool then 
        (let en = Eliom_content.Html.To_dom.of_div (div ~a:[a_id "treeResult"] [txt "A palavra é aceite"]) in
            Js_of_ocaml.Dom.appendChild textBox en)
      else 
        (let en = Eliom_content.Html.To_dom.of_div (div ~a:[a_id "treeResult"] [txt "A palavra não é aceite"]) in
            Js_of_ocaml.Dom.appendChild textBox en);
      let (right, wrong) = (StateVariables.returnRe())#countRightTrees !wordAsList in 
        let textt = "Existem " ^ (string_of_int (right)) ^ " derivações bem sucedidas: " in
          let en = Eliom_content.Html.To_dom.of_div (div ~a:[a_id "treeResult"] [txt textt]) in
            Js_of_ocaml.Dom.appendChild textBox en;
        let textt1 = "Existem " ^ (string_of_int (wrong)) ^ " derivações falhadas: " in
          let en1 = Eliom_content.Html.To_dom.of_div (div ~a:[a_id "treeResult"] [txt textt1]) in
            Js_of_ocaml.Dom.appendChild textBox en1

    let defineNumberTrees pos leng =
      let textBox = Dom_html.getElementById "textBox" in
          let textt = (string_of_int (pos)) ^ " de " ^ (string_of_int (leng)) in 
              let en = Eliom_content.Html.To_dom.of_div (div ~a:[a_id "treeResult"] [txt textt]) in
              Js_of_ocaml.Dom.appendChild textBox en

    let defineTreeButtons () =
      let textBox = Dom_html.getElementById "textBox" in
        let previousButton = Eliom_content.Html.To_dom.of_button (HtmlPageClient.previousTree()) in 
            Js_of_ocaml.Dom.appendChild textBox previousButton;
            let nextButton = Eliom_content.Html.To_dom.of_button (HtmlPageClient.nextTree()) in 
            Js_of_ocaml.Dom.appendChild textBox nextButton

    let nextTree () = 
      HtmlPageClient.twoBoxes ();
      HtmlPageClient.clearBox2();
      HtmlPageClient.putCy2Buttons();
      let blah = (StateVariables.returnRe())#next  in
        let text = (StateVariables.returnRe())#printTree blah in 
          JS.log (text);
          resultCount (StateVariables.returnResultTree());
          let pos = (StateVariables.returnRe())#position in 
          let leng = (StateVariables.returnRe())#length in 
            defineNumberTrees pos leng;
      defineTreeButtons ();  
      Graphics.startGraph3 (text)

      let previousTree () =
        let blah = (StateVariables.returnRe())#back  in
          let text = (StateVariables.returnRe())#printTree blah in 
          JS.log text;
          HtmlPageClient.twoBoxes ();
          Graphics.destroyGraph1();
          HtmlPageClient.putCy2Buttons ();
          resultCount (StateVariables.returnResultTree());
          let pos = (StateVariables.returnRe())#position in 
          let leng = (StateVariables.returnRe())#length in 
          defineNumberTrees pos leng;
          defineTreeButtons ();
          Graphics.startGraph3 (text)


    let acceptRegularExpression word = 
        StateVariables.changeCy2ToVerify ();
        let w = (StateVariables.returnRe())#stringAsList1 word in
        wordAsList := w;
        HtmlPageClient.clearBox2();
        HtmlPageClient.twoBoxes ();
        HtmlPageClient.putCy2Buttons ();
        (StateVariables.returnRe())#startAllTrees w;
        StateVariables.changeResultTree ((StateVariables.returnRe())#accept w);
        if (StateVariables.returnResultTree()) then
          (resultCount true;
          let blah = (StateVariables.returnRe())#getRightTrees in 
          let text = (StateVariables.returnRe())#printTree blah in 
          JS.log (text);
          let pos = (StateVariables.returnRe())#position in 
          let leng = (StateVariables.returnRe())#length in
          defineNumberTrees pos leng;
          defineTreeButtons ();
          Graphics.startGraph3 (text))
        else 
          (resultCount false;
          let blah = (StateVariables.returnRe())#getWrongTrees in 
          let text = (StateVariables.returnRe())#printTree blah in 
          JS.log (text);
          let pos = (StateVariables.returnRe())#position in 
          let leng = (StateVariables.returnRe())#length in
          defineNumberTrees pos leng;
          defineTreeButtons ();
          Graphics.startGraph3 (text))

    let automatonToRegExp() =
      if StateVariables.getCy1Type() = StateVariables.getRegexType() then
          JS.alert ("Já está a trabalhar com uma expressão regular")
      else
        (StateVariables.changeCy2ToRegex ();
      let reg = (StateVariables.returnAutomata())#toRegularExpression1 in
        let rep = reg#representation in 
        let re = new RegularExpressionAnimation.model (Representation (rep)) in
          defineRegularExpression1 re)


    let closeRight () =
      HtmlPageClient.oneBox();
      StateVariables.cleanCy2Type ();
      HtmlPageClient.clearBox2();
      if (StateVariables.getCy1Type() = StateVariables.getAutomatonType()) then
        Graphics.resetStyle()

    let fromExpressionToAutomaton () =
      if StateVariables.getCy1Type() = StateVariables.getAutomatonType() then 
          JS.alert ("Já está a trabalhar com um autómato finito")
      else
      (HtmlPageClient.twoBoxes ();
      HtmlPageClient.clearBox2();
      StateVariables.changeCy2ToAutomaton ();
      HtmlPageClient.putCy2Buttons ();
      Graphics.startGraph1();
      Graphics.fit();
      let auto = (StateVariables.returnRe())#toFiniteAutomaton in 
        let maton = auto#representation in
          StateVariables.changeAutomata1 (new FiniteAutomatonAnimation.model (Representation (maton)));
            (StateVariables.returnAutomata1())#drawExample1)

    let clearEnum() =
      if StateVariables.getCy2Type() = StateVariables.getEnumerationType() then
          StateVariables.cleanCy2Type ();
        HtmlPageClient.oneBox ();
        let element = Dom_html.getElementById "cy2" in
        element##.innerHTML := Js_of_ocaml.Js.string ""

    let accept word =
      if StateVariables.getCy1Type() = StateVariables.getAutomatonType() then
        acceptAutomaton word
      else 
        acceptRegularExpression word

    let addInitialNode node =
      if StateVariables.getCy1Type() = StateVariables.getAutomatonType() then
        let stateExists = Set.belongs node (StateVariables.returnAutomata())#representation.allStates in 
          StateVariables.changeAutomata ((StateVariables.returnAutomata())#addInitialNode node false stateExists);
        Graphics.destroyGraph();
        Graphics.startGraph();
        (StateVariables.returnAutomata())#drawExample
      else 
        (if StateVariables.getCy2Type() <> StateVariables.getEnumerationType() then
          (HtmlPageClient.oneBox());
        HtmlPageClient.clearBox1();
        StateVariables.changeCy1ToAutomaton ();
        HtmlPageClient.disableButtons (StateVariables.getCy1Type());
        HtmlPageClient.putCyAutomataButtons();
        StateVariables.changeAutomata ((StateVariables.returnAutomata())#addInitialNode node true false);
        Graphics.startGraph ();
        Graphics.createNode node true false);
      defineInformationBoxAutomaton ()
    
    let addFinalNode node =
      if StateVariables.getCy1Type() = StateVariables.getAutomatonType() then 
        (if (Set.belongs node (StateVariables.returnAutomata())#representation.allStates) then
          (Graphics.eliminateNode node;
          StateVariables.changeAutomata ( (StateVariables.returnAutomata())#addFinalNode node false true);
          Graphics.destroyGraph();
          Graphics.startGraph();
          (StateVariables.returnAutomata())#drawExample)
        else 
          (StateVariables.changeAutomata ( (StateVariables.returnAutomata())#addFinalNode node false false);
          Graphics.createNode node false true))
      else
        (if StateVariables.getCy2Type() <> StateVariables.getEnumerationType() then
          (HtmlPageClient.oneBox());
        HtmlPageClient.clearBox1();
        StateVariables.changeCy1ToAutomaton ();
        HtmlPageClient.disableButtons (StateVariables.getCy1Type());
        HtmlPageClient.putCyAutomataButtons();
        StateVariables.changeAutomata ( (StateVariables.returnAutomata())#addFinalNode node true false);
        Graphics.startGraph ();
        Graphics.createNode node true true);
      defineInformationBoxAutomaton ()
          
    let addNode node =
      if StateVariables.getCy1Type() = StateVariables.getAutomatonType() then
        (if (Set.belongs node (StateVariables.returnAutomata())#representation.allStates) then 
          (JS.alert (Js.string "O estado já existe"))
        else 
          (StateVariables.changeAutomata ( (StateVariables.returnAutomata())#addNode node false);
          Graphics.createNode node false false;
          defineInformationBoxAutomaton ()))
      else
        (if StateVariables.getCy2Type() <> StateVariables.getEnumerationType() then
          (HtmlPageClient.oneBox());
        HtmlPageClient.clearBox1();
        StateVariables.changeCy1ToAutomaton ();
        HtmlPageClient.disableButtons (StateVariables.getCy1Type());
        HtmlPageClient.putCyAutomataButtons();
        StateVariables.changeAutomata ( (StateVariables.returnAutomata())#addInitialNode node true false);
        Graphics.startGraph ();
        Graphics.createNode node true false;
        defineInformationBoxAutomaton ())

    let about () =
      HtmlPageClient.oneBox ();
      let cy = Dom_html.getElementById "regExp" in
      cy##.innerHTML := Js.string "";
      let test1 = Eliom_content.Html.To_dom.of_div (div [HtmlPageClient.closeLeft()]) in
      Js_of_ocaml.Dom.appendChild cy test1;
      StateVariables.changeCy1ToText();
      let info = Eliom_content.Html.To_dom.of_div (div ~a:[a_id "aboutBox"][
          h1 ~a:[a_id "aboutTitle"][txt "Sobre"];
          h2 ~a:[a_id "aboutsubTitle"] [txt "Sobre OFLAT"];
          div ~a:[a_id "aboutText"] [
            p [txt "Esta ferramenta está a ser desenvolvida na "; Raw.a ~a:[a_href (Raw.uri_of_string "http://nova-lincs.di.fct.unl.pt/")] [txt "NOVA-LINCS"]; txt " (no Departamento de Informática da FCT- UNL) pelo projecto "; 
                Raw.a ~a:[a_href (Raw.uri_of_string "https://release.di.ubi.pt/factor/index.html")] [txt "Factor"]; txt " e Co-financiado pela ";
                Raw.a ~a:[a_href (Raw.uri_of_string "https://tezos.com/")] [txt "fundação Tezos"]; txt "."];
            p [txt "Está a ser desenvolvida em OCaml através do framework Ocsigen, estando o código fonte disponível no ";  Raw.a ~a:[a_href (Raw.uri_of_string "https://github.com/git-amd/OFLAT")] [txt "GitHub"]; txt "."]
          ];
          h2 ~a:[a_id "aboutsubTitle"] [txt "Instruções"];
          div ~a:[a_id "aboutText"] [
            p [txt "Neste momento é possível trabalhar com Autómatos Finitos, Expressões Regulares e realizar exercícios."];
            p [txt "Para se importar um ficheiro este deve estar em txt ou JSON. O formato de um autómato é:"];
            pre [txt "
                      {
	                      kind : finite automaton,
	                      description : descrição do autómato,
	                      name : nome do autómato,
	                      alphabet : [lista de elementos],
	                      states : [lista de estados],
	                      initialState : um estado inicial,
                        transitions : [Lista de transições com formato [estado de partida, elemento do alfabeto, estado de chegada]],
                        acceptStates : [lista de estados]
                      }"
              ];
            p [txt "O formato de uma expressão é:"];
            pre [txt "
              {
	              kind : regular expression,
	              description : descrição da expressão regular,
	              name : nome da expressão regular,
	              re : expressão regular
              }"
            ];
            p [txt "O formato de um exercício é:"];
            pre [txt "
                {
	                kind : enumeration,
	                description : descrição do autómato,
	                name : nome do exercício,
	                problem : exercício apresentado ao aluno,
	                inside : [lista de palavras que devem ser aceites],
	                outside : [lista de palavras que não devem ser aceites]
                }"
            ];
            p[txt "Todos os elementos do lado direito dos dois pontos devem ser colocados entre aspas."];
            p [txt "É possivel também carregar exemplos do servidor."];
            p [txt "A expressão regular pode também ser criada escrevendo a mesma na caixa de texto e os autómatos pode ser criados passo a passo usando as caixas de seleção Estado e Transição. Para se criar um estado é necessário indicar o 
            nome do mesmo na caixa de texto e para se criar uma transição coloca-se na caixa de texto estado de partida, elemento do alfabeto, estado de chegada (exemplo: A a B)."]
            ]
        ]) in
      Js_of_ocaml.Dom.appendChild cy info 

      let feedback () = 
        HtmlPageClient.oneBox ();
        let cy = Dom_html.getElementById "regExp" in
        cy##.innerHTML := Js.string "";
        let test = Eliom_content.Html.To_dom.of_div (div [HtmlPageClient.closeLeft()]) in
        Js_of_ocaml.Dom.appendChild cy test;
        let test1 = Eliom_content.Html.To_dom.of_div (div ~a:[a_id "feedbackBox"] [
          h1 ~a:[a_id "feedbackTitle"] [txt "Feedback"];
          p ~a:[a_id "feedbackText"] [txt "De forma a criarmos uma página cada vez melhor agradecemos qualquer tipo de feedback."];
          p ~a:[a_id "feedbackText"] [
             span [txt "Se tiver alguma dúvida ou queira dar opinião, envie, por favor, um email para "];
            HtmlPageClient.mailtoClick();
            span [txt "."];
          ];
          p ~a:[a_id "feedbackText"] [txt "Obrigada!"]
        ]) in 

        Js_of_ocaml.Dom.appendChild cy test1

  end

  and HtmlPageClient: sig
    val closeLeft: unit -> [< Html_types.div_content_fun > `Button ] Eliom_content.Html.D.elt
    val changeDirection: unit -> [< Html_types.div_content_fun > `Button ] Eliom_content.Html.D.elt
    val closeRight: unit -> [< Html_types.div_content_fun > `Button ] Eliom_content.Html.D.elt
    val completeInfo: unit -> [< Html_types.div_content_fun > `Button ] Eliom_content.Html.D.elt
    val closeInfo: unit -> [< Html_types.div_content_fun > `Button ] Eliom_content.Html.D.elt
    val checkEnum: unit -> [< Html_types.div_content_fun > `Button ] Eliom_content.Html.D.elt
    val clearEnum: unit -> [< Html_types.div_content_fun > `Button ] Eliom_content.Html.D.elt
    val previousTree: unit -> [< Html_types.div_content_fun > `Button ] Eliom_content.Html.D.elt
    val nextTree: unit -> [< Html_types.div_content_fun > `Button ] Eliom_content.Html.D.elt

    val usefulStates: unit -> [< Html_types.div_content_fun > `Button ] Eliom_content.Html.D.elt
    val accessibleStates: unit -> [< Html_types.div_content_fun > `Button ] Eliom_content.Html.D.elt
    val productiveStates: unit -> [< Html_types.div_content_fun > `Button ] Eliom_content.Html.D.elt
    val clean: unit -> [< Html_types.div_content_fun > `Button ] Eliom_content.Html.D.elt
    val deter: unit -> [< Html_types.div_content_fun > `Button ] Eliom_content.Html.D.elt
    val mini: unit -> [< Html_types.div_content_fun > `Button ] Eliom_content.Html.D.elt

    val mailtoClick: unit -> [> [> Html_types.txt ] Html_types.a ] Eliom_content.Html.D.Raw.elt

    val getDeterminim: bool -> unit
    val getMinimism: bool -> unit
    val getHasUselessStates: bool -> states -> unit
    val getNumberStates: int -> unit
    val getNumberTransitions: int -> unit
    val defineInformationBoxAutomaton: unit -> unit

    val putCyAutomataButtons: unit -> unit
    val putButton: Eliom_lib.String.t -> unit
    val putCyREButtons: unit -> unit

    val defineRE: string -> unit

    val oneBox: unit -> unit
    val twoBoxes: unit -> unit
    val clearBox1: unit -> unit
    val clearBox2: unit -> unit
    val putCy2Buttons: unit -> unit
    val putCy2Text: string -> unit

    val putEnumButton: unit -> unit
    val addEnumTitle: unit -> unit
    val defineEnumProblem: string -> unit
    val defineResult: bool -> unit
    val addAcceptedTitle: unit -> unit
    val addNonAcceptTitle: unit -> unit
    val addEnumCheckButton: unit -> unit

    val putWords: OCamlFlat.words -> unit
    val createSpanList: OCamlFlat.symbol list -> string -> string -> unit

    val disableButtons: string -> unit


  end 
  = 

  struct
    let listOnlyAutomataButtons = ["backwards"; "step"; "forward"]
    let listOnlyExpressionButtons = []
    let listOtherButtons = ["testing"; "generate"]

    let createSpanList word acceptance list =
        let element = Dom_html.getElementById list in
        let string_of_word = String.concat "" (List.map (String.make 1) word) in
          let ac = Eliom_content.Html.To_dom.of_span (span ~a: [a_id acceptance][txt ("' " ^ string_of_word ^ " '")]) in
            Js_of_ocaml.Dom.appendChild element ac;
          let space = Eliom_content.Html.To_dom.of_br (br()) in
            Js_of_ocaml.Dom.appendChild element space

    let putCyREButtons() =
      let cy = Dom_html.getElementById "buttonBox" in
        cy##.innerHTML := Js_of_ocaml.Js.string "";
        let divOfButtons = Eliom_content.Html.To_dom.of_div (div [HtmlPageClient.closeLeft(); HtmlPageClient.changeDirection()]) in 
        Js_of_ocaml.Dom.appendChild cy divOfButtons

    let defineRE def =
      let regExp = Dom_html.getElementById "regExp" in
        let expr = Eliom_content.Html.To_dom.of_div (div [txt def]) in
          Js_of_ocaml.Dom.appendChild regExp expr;
      let infoBox = Dom_html.getElementById "infoBox" in
        infoBox##.innerHTML := Js_of_ocaml.Js.string ""

    let putWords listWords =
      let textBox = Dom_html.getElementById "textBox" in
          textBox##.innerHTML := Js_of_ocaml.Js.string "";
        let title = Eliom_content.Html.To_dom.of_div (div [txt "Palavras geradas:"]) in 
          Js_of_ocaml.Dom.appendChild textBox title;
          let zz = textarea ~a:[a_id "textarea"; a_rows 2; a_cols 20] (txt "") in 
          let i = (Eliom_content.Html.To_dom.of_textarea zz) in
          let text = Eliom_content.Html.To_dom.of_div (div [zz]) in
          Js_of_ocaml.Dom.appendChild textBox text;
      let test = Set.toList listWords in 
        let string_of_word w = "' " ^ String.concat "" (List.map (String.make 1) w) ^ " '" in
            let string_of_words l = String.concat ", " (List.map string_of_word l) in 
              let res = string_of_words test in
                i##.value:= Js_of_ocaml.Js.string res

    let putEnumButton () = 
      let buttonBox = Dom_html.getElementById "buttonBox1" in
        let clearButton = Eliom_content.Html.To_dom.of_button (HtmlPageClient.clearEnum()) in 
            Js_of_ocaml.Dom.appendChild buttonBox clearButton
    
    let addEnumTitle () =
      let textBox = Dom_html.getElementById "textBox" in
        let en = Eliom_content.Html.To_dom.of_h2 (h2 ~a: [a_id "enum"][txt "Exercício"]) in
            Js_of_ocaml.Dom.appendChild textBox en
    
    let defineEnumProblem prob =
      let textBox = Dom_html.getElementById "textBox" in
      let test =  "Problema: " ^ prob in
          let en = Eliom_content.Html.To_dom.of_div (div ~a: [a_id "prob"][txt test]) in
            Js_of_ocaml.Dom.appendChild textBox en;
      let resultBox = Eliom_content.Html.To_dom.of_div (div ~a: [a_id "resultBox"][txt ""]) in
        Js_of_ocaml.Dom.appendChild textBox resultBox

    let defineResult result =
      let resultBox = Dom_html.getElementById "resultBox" in
        resultBox##.innerHTML := Js_of_ocaml.Js.string "";
          if result then 
            let res = Eliom_content.Html.To_dom.of_div (div ~a:[a_id "right"][txt "Resposta correcta"]) in
            Js_of_ocaml.Dom.appendChild resultBox res
          else
            (let res = Eliom_content.Html.To_dom.of_div (div ~a:[a_id "error"][txt "Resposta não correcta"]) in
            Js_of_ocaml.Dom.appendChild resultBox res);
      let element = Dom_html.getElementById "inside" in
        element##.innerHTML := Js_of_ocaml.Js.string "";
      let element2 = Dom_html.getElementById "outside" in
        element2##.innerHTML := Js_of_ocaml.Js.string ""
      
      let addAcceptedTitle () =
        let textBox = Dom_html.getElementById "textBox" in
          let tac = Eliom_content.Html.To_dom.of_div (div ~a: [a_id "accept"][txt "Palavras que devem ser aceites: "]) in 
            Js_of_ocaml.Dom.appendChild textBox tac;
          let ac = Eliom_content.Html.To_dom.of_div (div ~a: [a_id "inside"][txt ""]) in
              Js_of_ocaml.Dom.appendChild textBox ac

      let addNonAcceptTitle () =
        let textBox = Dom_html.getElementById "textBox" in
        let twr = Eliom_content.Html.To_dom.of_div (div ~a: [a_id "wrong"][txt "Palavras que não devem ser aceites: "]) in 
            Js_of_ocaml.Dom.appendChild textBox twr;
          let wr = Eliom_content.Html.To_dom.of_div (div ~a: [a_id "outside"][txt ""]) in
              Js_of_ocaml.Dom.appendChild textBox wr

      let addEnumCheckButton () =
        let textBox = Dom_html.getElementById "textBox" in
          let checkButton = Eliom_content.Html.To_dom.of_button (HtmlPageClient.checkEnum()) in 
            Js_of_ocaml.Dom.appendChild textBox checkButton
    
    let clearBox1 () = 
      let element2 = Dom_html.getElementById "regExp" in
        element2##.innerHTML := Js_of_ocaml.Js.string ""
    
    let disableButton buttonName =
      let buttonTo = Dom_html.getElementById buttonName in
      buttonTo##setAttribute (Js.string "disabled") (Js.string "disabled")
    
    let enableButton buttonName =
      let buttonTo = Dom_html.getElementById buttonName in
      buttonTo##removeAttribute (Js.string "disabled")

    let disableButtons type1 =
      if type1 = "finite automaton" then
        (* (List.iter (fun el -> disableButton el) listOnlyExpressionButtons; *)
        (List.iter (fun el -> enableButton el) listOnlyAutomataButtons)
      else 
        (List.iter (fun el -> disableButton el) listOnlyAutomataButtons)
        (* List.iter (fun el -> enableButton el) listOnlyExpressionButtons) *)

      let enableAllButtons () =
         List.iter (fun el -> enableButton el) listOnlyAutomataButtons;
         List.iter (fun el -> enableButton el) listOnlyExpressionButtons

    let oneBox () = 
      let box1 = Dom_html.getElementById "Box1" in
      box1##.style##.width:= Js_of_ocaml.Js.string "100%";
      let box2 = Dom_html.getElementById "Box2" in
      box2##.style##.width:= Js_of_ocaml.Js.string "0%";
      let buttonBox1 = Dom_html.getElementById "buttonBox1" in
      buttonBox1##.innerHTML := Js_of_ocaml.Js.string "";
      StateVariables.completeSize();
      Graphics.destroyGraph1();
      Graphics.fit()

    let clearBox2 () = 
        let buttonBox1 = Dom_html.getElementById "buttonBox1" in
          buttonBox1##.innerHTML := Js_of_ocaml.Js.string "";
        let textBox = Dom_html.getElementById "textBox" in
          textBox##.innerHTML := Js_of_ocaml.Js.string "";
        Graphics.destroyGraph1()

    let twoBoxes () = 
      clearBox2();
      let box1 = Dom_html.getElementById "Box1" in
        box1##.style##.width:= Js_of_ocaml.Js.string "49.5%";
      let box2 = Dom_html.getElementById "Box2" in
        box2##.style##.width:= Js_of_ocaml.Js.string "49.5%";
        StateVariables.halfSize();
        Graphics.fit()

    let getDeterminim deter = 
      let infoBox = Dom_html.getElementById "infoBox" in
        if deter then 
          let deterministic = Eliom_content.Html.To_dom.of_div (div ~a:[a_id "isdeterministic"] [txt "O Autómato é determinista."]) in
            Js_of_ocaml.Dom.appendChild infoBox deterministic
        else 
          let deterministic = Eliom_content.Html.To_dom.of_div (div ~a:[a_id "isdeterministic"] [txt "O Autómato não é determinista."]) in
            Js_of_ocaml.Dom.appendChild infoBox deterministic
    
    let getMinimism min = 
      let infoBox = Dom_html.getElementById "infoBox" in
          if min then 
          let minimal = Eliom_content.Html.To_dom.of_div (div ~a:[a_id "isminimal"] [txt "O autómato é minimo."]) in 
          Js_of_ocaml.Dom.appendChild infoBox minimal 
        else 
          let minimal = Eliom_content.Html.To_dom.of_div (div ~a:[a_id "isminimal"] [txt "O autómato não é minimo."]) in 
          Js_of_ocaml.Dom.appendChild infoBox minimal
    
    let getHasUselessStates use uStates = 
      let infoBox = Dom_html.getElementById "infoBox" in
          if use then 
            (let useful = Eliom_content.Html.To_dom.of_div (div ~a:[a_id "areuseful"] [txt "O autómato não tem estados inúteis."]) in 
            Js_of_ocaml.Dom.appendChild infoBox useful)
          else 
          (let useless = Set.toList uStates in
            let number = List.length useless in 
              let string_of_words l = String.concat ", " l in
                let blah = string_of_words useless in
                let useful = "O autómato tem " ^ (string_of_int number) ^ " estados inúteis: " ^ blah in 
                  let use = Eliom_content.Html.To_dom.of_div (div ~a:[a_id "areuseful"] [txt useful]) in 
                  Js_of_ocaml.Dom.appendChild infoBox use)

    let getNumberStates nStates = 
      let number = string_of_int nStates in 
        let infoBox = Dom_html.getElementById "infoBox" in
          let sentence = "Número de Estados: " ^ number in 
            let sentence1 = Eliom_content.Html.To_dom.of_div (div ~a:[a_id "numberstates"] [txt sentence]) in
              Js_of_ocaml.Dom.appendChild infoBox sentence1
    
    let getNumberTransitions nTransitions = 
      let number = string_of_int nTransitions in 
        let infoBox = Dom_html.getElementById "infoBox" in
          let sentence = "Número de Transições: " ^ number in 
            let sentence1 = Eliom_content.Html.To_dom.of_div (div ~a:[a_id "numbertransitions"] [txt sentence]) in
              Js_of_ocaml.Dom.appendChild infoBox sentence1
    
    let defineInformationBoxAutomaton () =
      let infoBox = Dom_html.getElementById "infoBox" in
        infoBox##.innerHTML := Js_of_ocaml.Js.string "Dados sobre o Autómato"

    let changeDirection_handler = 
      (fun _ ->
          Graphics.changeDirection();
      )
    
    let changeDirection() = button ~a:[a_id "changeDirection"; a_onclick changeDirection_handler] [txt "Mudar direção da árvore"]

    let rec closeLeft_handler = 
      (fun _ ->
        Controller.change()
      )         

    and closeLeft() = button ~a:[a_id "closeLeft"; a_onclick closeLeft_handler] [txt "X"]

    let closeRight_handler = 
        (fun _ ->
          Controller.closeRight()
        )

    let closeRight() = button ~a:[a_id "closeRight"; a_onclick closeRight_handler] [txt "X"]

    let infoClose_handler = 
      (fun _ ->
        Controller.closeCompleteAutomaton()
      )

  let closeInfo() = button ~a:[a_onclick infoClose_handler] [txt "X"]

    let info_handler = 
      (fun _ ->
        Controller.getCompleteAutomaton()
      )         

    let completeInfo() = button ~a:[a_id "formatting"; a_onclick info_handler] [txt "Ver formatação do Autómato"]


    let checkEnum_handler = 
      (fun _ ->
        Controller.check ()
      )         

    let checkEnum() = button ~a:[a_onclick checkEnum_handler] [txt "Verificar"]

    let clearEnum_handler = 
      (fun _ ->
        Controller.clearEnum()
      )         

    let clearEnum() = button ~a:[a_onclick clearEnum_handler] [txt "X"]

    let nextTree_handler = 
      (fun _ ->
        Controller.nextTree()
      )         

    let nextTree () = button ~a:[a_onclick nextTree_handler] [txt "Proxima"]

    let previousTree_handler = 
      (fun _ ->
        Controller.previousTree()
      )         

    let previousTree () = button ~a:[a_onclick previousTree_handler] [txt "Anterior"]

    let minimize_handler = 
      (fun _ ->
        Controller.defineMinimized()
      )

    let mini() = button ~a:[a_id "minimize"; a_onclick minimize_handler] [txt "Minimizar Autómato"]

    let toDeterministic_handler = 
      (fun _ ->
        Controller.getDeterministic()
      )         

    let deter() = button ~a:[a_id "deterministic"; a_onclick toDeterministic_handler] [txt "Determininizar Autómato"]

    let clean_handler = 
      (fun _ ->
        Controller.cleanUseless()
      )         

    let clean() = button ~a:[a_id "clean"; a_onclick clean_handler] [txt "Limpar"]

    let productiveStates_handler = 
      (fun _ ->
        Controller.paintAllProductives ()
      )

    let productiveStates() = button ~a:[a_id "productive"; a_onclick productiveStates_handler] [txt "Estados Produtivos"]
  
    let accessibleStates_handler = 
      (fun _ ->
        Controller.paintAllReachable ()
      )
    
    let accessibleStates() = button ~a:[a_id "accessible"; a_onclick accessibleStates_handler] [txt "Estados Acessiveis"]

    let usefulStates_handler = 
      (fun _ ->
        Controller.paintAllUseful ()
      )

    let usefulStates() = button ~a:[a_id "useful"; a_onclick usefulStates_handler] [txt "Estados Úteis"]

    let stringBlah = "mailto:rp.macedo@campus.fct.unl.pt"

    let mailtoClick() = Raw.a ~a:[a_href (Raw.uri_of_string stringBlah)] [txt "Rita Macedo"]

    let putCyAutomataButtons () =
        let buttonBox = Dom_html.getElementById "buttonBox" in
          buttonBox##.innerHTML := Js_of_ocaml.Js.string "";
        let buttonClose = Eliom_content.Html.To_dom.of_div (div [closeLeft(); completeInfo()]) in    
          Js_of_ocaml.Dom.appendChild buttonBox buttonClose;
        let actionButtons = Eliom_content.Html.To_dom.of_div (div[div[clean(); deter(); mini()]; div [productiveStates(); accessibleStates(); usefulStates()]]) in
          Js_of_ocaml.Dom.appendChild buttonBox actionButtons

    let putCy2Buttons () = 
      let buttonBox = Dom_html.getElementById "buttonBox1" in
          let test1 = Eliom_content.Html.To_dom.of_div (div [HtmlPageClient.closeRight()]) in 
            Js_of_ocaml.Dom.appendChild buttonBox test1

    let putCy2Text text = 
      let textBox = Dom_html.getElementById "textBox" in
        let expr = Eliom_content.Html.To_dom.of_div (div [txt text]) in
          Js_of_ocaml.Dom.appendChild textBox expr

    let createServerExampleButton name =
    button ~a:[a_id "exampleButton"; a_onclick (fun _ ->
        Lwt.ignore_result (
          let%lwt str = Server.getExample name in
          Lwt.return (Controller.createText (Js.string str))
        )         
      )] [txt name]

  let putButton name = 
    if (String.equal name ".DS_Store") then ()
    else 
    (let examples = Dom_html.getElementById "examplesServer" in
    let title = name in 
    let example = Eliom_content.Html.To_dom.of_button (createServerExampleButton title) in 
    Js_of_ocaml.Dom.appendChild examples example)

  end 

  (** ------------------------------------------------------------------------
 * --- Local file reader widget --------------------------------------------
 * -------------------------------------------------------------------------
 *
 *   The main function is 'fileWidgetMake'. This function creates an
 *    <input type="file"> element supporting the interactive selection
 *    and loading of a local text file.
 *)

module FileReaderController
 =
  struct
  let fileWidgetCanceled () =
	  JS.alert "Canceled"

  let fileWidgetAction txt =
	  Controller.createText txt;
    Controller.printErrors ()

  let onFileLoad e =
	  Js.Opt.case
		  (e##.target)
		  (fun () -> Js.bool false)
		  (fun target ->
			  Js.Opt.case
				  (File.CoerceTo.string target##.result)
				  (fun () -> Js.bool false)
				  (fun data -> fileWidgetAction data; Js.bool false)
		  )

  let fileWidgetHandle filewidget =
	  Js.Optdef.case
		  (filewidget##.files)
		  (fileWidgetCanceled)
		  (fun files ->
			  Js.Opt.case
				  (files##item 0)
				  (fileWidgetCanceled)
				  (fun file -> 
					  let reader = new%js File.fileReader in
						  reader##.onload := Dom.handler onFileLoad;
						  reader##readAsText (Js.Unsafe.coerce file)
		  )		)

  let fileWidgetEvents filewidget () =
	  let fw = Eliom_content.Html.To_dom.of_input filewidget in
		  Js_of_ocaml_lwt.Lwt_js_events.changes
			  fw
			  (fun _ _ -> fileWidgetHandle fw; Lwt.return ())

  end 
(* --------------------------------------------------------------------------
 * -------------------------------------------------------------------------- *)
]

module HtmlPage
 = 

  struct 

  let fileWidgetMake () =
	let filewidget = input ~a: [a_id "file_input"; a_input_type `File] () in
	let _ = [%client (Lwt.async (FileReaderController.fileWidgetEvents ~%filewidget): unit)] in
		filewidget

  let inputBox = input ~a:[a_id "box"; a_input_type `Text]()

  let completeSentence_handler = [%client (fun _ ->
    let i = (Eliom_content.Html.To_dom.of_input ~%inputBox) in
    let v = Js_of_ocaml.Js.to_string i##.value in
      Controller.accept v
    )]

  let stepbystep_handler = [%client (fun _ ->
        let i = (Eliom_content.Html.To_dom.of_input ~%inputBox) in
        let v = Js_of_ocaml.Js.to_string i##.value in
        Controller.startStep v;
        let i0 = (Eliom_content.Html.To_dom.of_input ~%inputBox) in
        i0##.value:= Controller.getNewSentence()
      )]
  
  let forward_handler = [%client (fun _ ->
        Controller.nextStep();
        let i0 = (Eliom_content.Html.To_dom.of_input ~%inputBox) in
        i0##.value:= Controller.getNewSentence()
      )]
      
  let backwards_handler = [%client (fun _ ->
        Controller.backStep();
        let i0 = (Eliom_content.Html.To_dom.of_input ~%inputBox) in
        i0##.value:= Controller.getNewSentence()
      )]

  let generateWords_handler = 
    [%client (fun _ ->
          let k = (Eliom_content.Html.To_dom.of_input ~%inputBox) in 
          let v = Js_of_ocaml.Js.to_string k##.value in
            let size = int_of_string v in 
          Controller.getWords size
            
      )]

  let defineRegExp_handler =
    [%client (fun _ ->
        let i = (Eliom_content.Html.To_dom.of_input ~%inputBox) in
        let v = Js_of_ocaml.Js.to_string i##.value in
        let re = new RegularExpressionAnimation.model (Representation (RegExpSyntax.parse v)) in 
        Controller.defineRegularExpression re;
        i##.value:= Js_of_ocaml.Js.string ""
      )]

  let serverexamples_handler = 
    [%client (fun _ ->
        let examples = Dom_html.getElementById "examplesServer" in
        examples##.innerHTML := Js_of_ocaml.Js.string "";
        Lwt.ignore_result (
          let%lwt lis = Server.getExamplesList () in
          List.iter (fun el -> HtmlPageClient.putButton el) lis;
          Lwt.return ()
        )
    )]

  let selectTransition_handler = 
    [%client (fun _ ->
      let i1 = (Eliom_content.Html.To_dom.of_input ~%inputBox) in
            let split = " " in
            let charSplit = String.get split 0 in 
            let v1 = Js_of_ocaml.Js.to_string i1##.value in
      let x = Eliom_content.Html.Of_dom.of_element (Dom_html.getElementById "selectTrans") in 
        let y = Eliom_content.Html.To_dom.of_select x in
      if v1 = "" then 
        JS.alert ("Adicione os estados e transições na caixa de texto")
      else 
        (
        let stringLength = String.length v1 in
        if (stringLength < 5) then
          JS.alert ("O formato de uma transição é: Estado Transição Estado")
        else 
        (let listOfThings = String.split_on_char charSplit v1 in 
        let c1 = List.nth listOfThings 0 in
        let c2 = List.nth listOfThings 2 in 
        let c3 = List.nth listOfThings 1 in
          let c4 = String.get c3 0 in

        let z = y##.selectedIndex in
        let w = Js_of_ocaml.Js.to_string (y##.value) in 
        if z = 1 && w = "Adicionar" then
            Controller.createTransition (c1, c4, c2)
        else 
          if z = 2 && w = "Apagar" then
            Controller.eliminateTransition (c1, c4, c2)
        ));
      i1##.value:= Js_of_ocaml.Js.string "";
      y##.selectedIndex := 0
    )]

  let selectNode_handler = 
    [%client (fun _ ->
    let i = (Eliom_content.Html.To_dom.of_input ~%inputBox) in
      let v = Js_of_ocaml.Js.to_string i##.value in
      let x = Eliom_content.Html.Of_dom.of_element (Dom_html.getElementById "selectNode") in 
        let y = Eliom_content.Html.To_dom.of_select x in
      if v = "" then 
        JS.alert ("Tem de dar um nome ao estado na caixa de texto")
      else 
        (
        let z = y##.selectedIndex in
        let w = Js_of_ocaml.Js.to_string (y##.value) in 
        if z = 1 && w = "Adicionar" then
            Controller.addNode v
        else 
          if z = 2 && w = "Inicial" then
            Controller.addInitialNode v
          else
            if z = 3 && w = "Final" then
              Controller.addFinalNode v
            else 
              if z = 4 && w = "Apagar" then
                Controller.eliminateNode v
        );
      i##.value:= Js_of_ocaml.Js.string "";
      y##.selectedIndex := 0
      )]

  let selectConversions_handler =
    [%client (fun _ ->
      let x = Eliom_content.Html.Of_dom.of_element (Dom_html.getElementById "selectConversion") in 
        let y = Eliom_content.Html.To_dom.of_select x in
        let z = y##.selectedIndex in
        let w = Js_of_ocaml.Js.to_string (y##.value) in 
        if z = 1 && w = "Expressão Regular" then
          (Controller.automatonToRegExp ())
        else 
          (if z = 2 && w = "Autómato" then
            Controller.fromExpressionToAutomaton ());
        y##.selectedIndex := 0
    )]

  let about_handler = [%client (fun _ -> Controller.about ())]

  let feedback_handler = [%client (fun _ -> Controller.feedback ())]


  (** --------------------- Buttons ------------------------------ **)
  let completeSentence = button ~a:[a_id "testing"; a_onclick completeSentence_handler] [txt "Testar frase completa"]

  let step_by_step = button ~a:[a_id "step"; a_onclick stepbystep_handler] [txt "Start"]

  let forward = button ~a:[a_id "forward"; a_onclick forward_handler] [txt "▶︎"]

  let backwards = button ~a:[a_id "backwards"; a_onclick backwards_handler] [txt "◀︎"]

  let words = button ~a:[a_id "generate"; a_onclick generateWords_handler] [txt "gerar palavras de tamanho x"]

  let defineRegExp = button ~a:[a_id "define"; a_onclick defineRegExp_handler] [txt "Definir Expressão Regular"]
    
  let serverExamples = button ~a:[a_id "server"; a_onload serverexamples_handler] [txt "Exemplos do Servidor"]

  let selectTransitions = 
    select ~a:[a_id "selectTrans"; a_class ["selectBoxes"]; a_onchange selectTransition_handler][
      option ~a:[a_selected ()] (txt "Transição");
      option (txt "Adicionar");
      option (txt "Apagar")
    ]

  let selectNode =
    select ~a:[a_id "selectNode"; a_class ["selectBoxes"]; a_onchange selectNode_handler][
      option ~a:[a_selected ()] (txt "Estado");
      option (txt "Adicionar");
      option (txt "Inicial");
      option (txt "Final");
      option (txt "Apagar")
    ]

  let selectConvert = 
    select ~a:[a_id "selectConversion"; a_class ["selectBoxes"]; a_onchange selectConversions_handler][
      option ~a:[a_selected ()] (txt "Converter");
      option (txt "Expressão Regular");
      option (txt "Autómato")
    ]

  let about = button ~a:[a_id "about"; a_onclick about_handler] [txt "Sobre"]

  let feedback = button ~a:[a_id "feedback"; a_onclick feedback_handler] [txt "Feedback"] 

end

let () =
  OFlat_app.register
    ~service:main_service
    (fun () () ->
      let open Eliom_content.Html.D in
      Lwt.return
         (html
            (head (title (txt "Autómatos Animados")) [script ~a:[a_src script_uri1] (txt ""); 
                                                      script ~a:[a_src script_uri5] (txt "");
                                                      script ~a:[a_src script_uri7] (txt "");  
                                                      script ~a:[a_src script_uri8] (txt ""); 
                                                      script ~a:[a_src script_uri9] (txt ""); 
                                                      css_link ~uri: (make_uri (Eliom_service.static_dir ()) ["codecss.css"]) ();
                                                      script ~a:[a_src script_uri] (txt "");
                                                    ])
            (body [div ~a:[a_class ["sidenav"]] [
                      div [h2 ~a: [a_id "title"] [txt "OFLAT"]; p ~a: [a_id "version"][txt "version 1.1"]]; 
                      div [HtmlPage.about];
                      div [HtmlPage.feedback];
                      hr();
                      div ~a: [a_id "inputTitle"] [txt "Importar exemplos:"];
                      div ~a:[a_id "fromFilesystem"][HtmlPage.fileWidgetMake ()];
                      div [HtmlPage.serverExamples];
                      div ~a:[a_id "examplesServer"] [];
                      hr();
                      div ~a: [a_id "inputTitle"] [txt "Input:"];
                      div ~a: [a_id "input"] [HtmlPage.inputBox];
                      div [HtmlPage.selectNode];
                      div [HtmlPage.selectTransitions];
                      div [HtmlPage.defineRegExp];
                      div [HtmlPage.words];
                      div [HtmlPage.completeSentence];
                      div [p ~a:[a_id "passo"] [txt "Aceitação passo-a-passo da palavra"]];
                      div [HtmlPage.backwards; HtmlPage.step_by_step; HtmlPage.forward];   
                      hr();
                      div [HtmlPage.selectConvert];    
                  ];
                  div ~a:[a_class ["main"]][
                        div ~a: [a_id "mainTitle"] [h1 [txt "Autómatos Animados"]];
                        div ~a: [a_class ["test"]][
                        div ~a:[a_id "Box1"] [div ~a:[a_id "buttonBox"] []; div ~a: [a_id "regExp"] []; div ~a:[a_id "cy"][]];
                        div ~a:[a_id "Box2"] [div ~a:[a_id "buttonBox1"] []; div ~a: [a_id "textBox"] []; div ~a:[a_id "cy2"] []];
                        div ~a:[a_id "infoBox"][]
                      ]
                  ];
					        footer ~a: [a_class ["footer"]] [txt "Desenvolvido em "; lincs_service; txt " pelo projeto "; factor_service; txt  "/ Co-financiado por "; tezos_service]
            ])
          )    
    )






















