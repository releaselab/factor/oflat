5656(** OFLAT - A graphical interface for the OCaml-Flat library written using the Ocsigen framework
 * 
 * @version v 0.2
 * 
 * @author Rita Macedo <rp.macedo@campus.fct.unl.pt>  [main author]
 * @author Joao Goncalves <jmg.goncalves@campus.fct.unl.pt>
 * @author Artur Miguel Dias <amd@fct.unl.pt>
 * @author Antonio Ravara <aravara@fct.unl.pt>
 * 
 * LICENCE - As now this is a private project, but later it will be open source.
 *  
*)

(** --- Js_of_ocaml methods --- **)

[%%shared
    open Eliom_lib
    open Eliom_content
    open Html.D
    open OCamlFlatSupport
    open OCamlFlat
    open Js_of_ocaml
    open CSS
]

[%%client

module JS = 
  struct 

    let global =
	    Js_of_ocaml.Js.Unsafe.global

    let eval s =
	    Js_of_ocaml.Js.Unsafe.eval_string s
    
    let exec s =
	    ignore (eval s)

    let console =
	    Js_of_ocaml.Firebug.console

    let string s =
	    Js_of_ocaml.Js.string s

      let log j =
	    ignore (console##log j)

    let alert j =
	    ignore (global##alert j)

  end 

(** --- Functions to call all the Javascript functions --- **)
module Graphics
  =
  struct

    (** Function to paint the states of the automaton in the cy div **)
    let paintNode node color = 
        JS.exec ("paintNode('" ^ node ^ "', '"^ color ^"')")

    (** Function to paint the states of the automaton in the cy2 div **)
    let paintNode1 node color = 
        JS.exec ("paintNode1('" ^ node ^ "', '"^ color ^"')")

    (** Function to create edge on the div cy **)
    let createEdge (f, s, t) = 
        JS.exec ("makeEdge('" ^ f ^ "', '" ^ t ^ "', '" ^ (String.make 1 s) ^ "')")

    (** Function to eliminate edge on the div cy **)
    let eliminateEdge (f, s, t) = 
        JS.exec ("removeEdge('" ^ f ^ "', '" ^ t ^ "', '" ^ (String.make 1 s) ^ "')")

    (** Function to create edge on the div cy2 **)
    let createEdge1 (f, s, t) = 
        JS.exec ("makeEdge2('" ^ f ^ "', '" ^ t ^ "', '" ^ (String.make 1 s) ^ "')")

    (** Function to create state on the div cy **)
    let createNode node isStart isFinal =
        JS.exec ("makeNode('" ^ node ^ "', '" ^ string_of_bool (isStart) ^ "', '" ^ string_of_bool (isFinal) ^ "')")

    (** Function to eliminate state on the div cy **)
    let eliminateNode node =
        JS.exec ("removeNode('" ^ node ^ "')")

    (** Function to create state on the div cy2 **)
    let createNode1 node isStart isFinal =
        JS.exec ("makeNode2('" ^ node ^ "', '" ^ string_of_bool (isStart) ^ "', '" ^ string_of_bool (isFinal) ^ "')")
    
    (** Function to start a new automaton in the div cy **)
    let startGraph () =   
        JS.exec ("start()")
    
    (** Function to start a new automaton in the div cy2 **)
    let startGraph1 () =   
        JS.exec ("start2()")

    (** Function to start a new Tree in the div cy **)
    let startGraph2 (nString) =   
        JS.exec ("startTree('" ^ nString ^ "')")

    (** Function to start a new Tree in the div cy2 **)
    let startGraph3 (nString) =   
        JS.exec ("startTree1('" ^ nString ^ "')")

    (** Function to eliminate the automaton/tree in the div cy **)
    let destroyGraph () =   
        JS.exec ("destroy1()")

    (** Function to eliminate the automaton/tree in the div cy2 **)
    let destroyGraph1 () =   
        JS.exec ("destroy2()")

    (** Clear all the color changes made to the automaton **)
    let resetStyle () = 
        JS.exec ("resetStyle()")

    (** Adjust the automaton to the size of the div cy **)
    let fit () =   
        JS.exec ("fit()")

    (** Generate a random color - used when the automaton is minimized and has the need of more colors than the pre-defined ones **)
    let getRandom() = 
        let test = Random.int 16777215 in
        Printf.sprintf "#%06x" test
    
    (** Used when the user wants to change the direction of the regular Expression tree **)
    let changeDirection () =
      JS.exec ("changeDirection()")

  end

  (** Module used to animate the finite automata  **)
module rec FiniteAutomatonAnimation : sig
	type transition = state * symbol * state
	type transitions = transition set
	type t = FiniteAutomaton.t
	val modelDesignation: unit -> string (* a funtion required for module recursive call *)
	class model :
	  t JSon.alternatives ->
		  object

      method kind : string
			method description : string
			method name : string
			method errors : string list
			method handleErrors : unit
			method validate : unit
			method toJSon: json
			
			method tracing : unit
			method accept : word -> bool
      method acceptBreadthFirst: word -> bool
			method generate : int -> words
      method generateUntil : int -> words
			method reachable : state -> states
			method productive : states

			method isDeterministic : bool
			method toDeterministic : FiniteAutomaton.model
      method toDeterministic1: FiniteAutomatonAnimation.model
			method getUsefulStates : states
			method getUselessStates : states
			method areAllStatesUseful: bool
			method cleanUselessStates: FiniteAutomaton.model
      method cleanUselessStates1: FiniteAutomatonAnimation.model
			method isMinimized : bool
			method minimize : FiniteAutomaton.model
      method minimize1 : FiniteAutomatonAnimation.model
      method equivalencePartition: states set
			method toRegularExpression : RegularExpression.model
      method toRegularExpression1 : RegularExpression.model

			method representation : FiniteAutomaton.t

      val mutable position : int
      val mutable steps : state Set.t array
      val mutable isOver : bool

      method changeSentence: unit -> unit
      method inputEdges: unit
      method inputEdges1: unit
      method inputNodes : unit
      method inputNodes1: unit
      method next: unit
      method paint: OCamlFlat.state -> int -> bool -> bool -> unit
      method paintStates: int -> OCamlFlat.state OCamlFlatSupport.Set.t -> bool -> unit
      method accept3: word -> bool Lwt.t
      method startAccept: unit
      method back: unit
      method drawExample: unit
      method drawExample1: unit

      method addInitialNode: OCamlFlat.state -> bool -> bool -> FiniteAutomatonAnimation.model
      method addNode: OCamlFlat.state -> bool -> FiniteAutomatonAnimation.model
      method addFinalNode: OCamlFlat.state -> bool -> bool -> FiniteAutomatonAnimation.model
      method eliminateNode: OCamlFlat.state -> bool -> bool -> FiniteAutomatonAnimation.model

      method newTransition: state * symbol * state -> FiniteAutomatonAnimation.model
      method eliminateTransition: OCamlFlat.state * OCamlFlat.symbol * OCamlFlat.state -> FiniteAutomatonAnimation.model
      
      method productivePainting: unit
      method reachablePainting: unit
      method usefulPainting: unit
      
      method stringAsList1: string -> char list
      method changeTheTestingSentence: string -> unit
      method newSentence1: string
      method paintMinimization: string array -> unit
      method numberStates: int
      method numberTransitions: int
      method getColors:int
      method drawMinimize: string array -> int -> unit
      method inputNodesPainting: string array -> int -> unit
      method checkEnumeration : Enumeration.enum -> bool
      method checkEnumerationFailures: OCamlFlat.Enumeration.enum -> OCamlFlat.words * OCamlFlat.words
		  end
end
   = 
  struct

  type transition =
		state	(* state *)
	  * symbol	(* consumed input symbol *)
	  * state	(* next state *)

	type transitions = transition set

	type t = FiniteAutomaton.t

	let modelDesignation () = "finite automaton"	

  (** Auxiliar Methods **)
  let productiveColor = "orange"
  let reachableColor = "yellow"
  let usefulColor = "purple"
  let stepState = "blue"
  let acceptState = "green"
  let wrongFinalState = "red"

  let paintProductive state =
        Graphics.paintNode state productiveColor

  let paintReachable state =
        Graphics.paintNode state reachableColor

  let paintUseful state =
        Graphics.paintNode state usefulColor

  let paintMinimization state color = 
        Graphics.paintNode state color
  
  let paintMinimized state color = 
        Graphics.paintNode1 state color

  let getMinStates list color = 
    Set.iter (fun el -> paintMinimization el color; JS.log (el)) list

  let rec intersection l1 l2 =
     match l1 with
        [] -> []
      | x::xs -> (if List.mem x l2 then [x] else []) @ intersection xs l2

  let iterateList meth list =
    List.iter (fun el -> (meth el) ) list

  let cut s = (String.get s 0, String.sub s 1 ((String.length s)-1)) ;;
    
  let rec stringAsList s =
        if s = "" then []
        else
            let (x,xs) = cut s in
                x::stringAsList xs
    ;;

  let sentence: char list ref = ref []

  let newSentence = ref ""

  let rec delay n = if n = 0 then Lwt.return ()
                                  else Lwt.bind (Lwt.pause()) (fun () -> delay (n-1))
  
  let transitionGet3 trns = Set.map ( fun (_,_,c) -> c ) trns
  
  let nextEpsilon2 st ts =
        let trns = Set.filter (fun (a,b,c) -> st = a && b = epsilon) ts in
        let nextStates = transitionGet3 trns in	
			Set.add st nextStates 

  let rec closeEmpty sts t = 
		let ns = Set.flatMap (fun st -> nextEpsilon2 st t) sts in
			if (Set.subset ns sts) then ns else closeEmpty (Set.union sts ns) t 

  let nextEpsilon1 st t =
          let n = Set.filter (fun (a,b,c) -> st = a && b = epsilon) t in
                  Set.map ( fun (_,_,d) -> d ) n		
				
				let rec nextEpsilons currsts t = 
					let ns = Set.flatMap (fun nst -> nextEpsilon1 nst t) currsts in
						if (Set.subset ns currsts) then ns else nextEpsilons (Set.union currsts ns) t
						
        let nextStates st sy t =
          let n = Set.filter (fun (a,b,c) -> st = a && sy = b) t in
                  Set.map ( fun (_,_,d) -> d) n

  let transition sts sy t = 
        let nsts = Set.flatMap (fun st -> nextStates st sy t) sts in
          Set.union nsts (nextEpsilons nsts t)

  class model (arg: FiniteAutomaton.t JSon.alternatives) =
		object(self) inherit FiniteAutomaton.model arg as super

      val mutable steps = [||]

      val mutable position = 0

      val mutable isOver = false

      method changeSentence () = 
        newSentence := "";
        let bar = '|' in 
          if position == 0 then
            (newSentence:= !newSentence ^ String.make 1 bar;
            for i = 0 to (List.length !sentence) - 1 do 
              newSentence:= !newSentence ^ String.make 1 (List.nth !sentence i);
            done)
          else 
            (for i = 0 to position - 1 do
              newSentence:= !newSentence ^ String.make 1 (List.nth !sentence i);
            done;
            newSentence:= !newSentence ^ String.make 1 bar;
            for i = position to (List.length !sentence) - 1 do 
              newSentence := !newSentence ^ String.make 1 (List.nth !sentence i);
            done)

      method inputNodes  = 
        Set.iter (fun el -> (Graphics.createNode el (el = self#representation.initialState) (Set.belongs el self#representation.acceptStates)) ) self#representation.allStates

      method inputNodes1  = 
        Set.iter (fun el -> (Graphics.createNode1 el (el = self#representation.initialState) (Set.belongs el self#representation.acceptStates)) ) self#representation.allStates

      method inputNodesPainting colors number = 
        let listStates = Set.toList self#representation.allStates in 
        for i=0 to number-1 do
          let newState = List.nth listStates i in 
          Graphics.createNode1 newState (newState = self#representation.initialState) (Set.belongs newState self#representation.acceptStates);
          let color = Array.get colors i in
          paintMinimized newState color
        done
      

      method inputEdges  = Set.iter (fun el -> (Graphics.createEdge el) ) self#representation.transitions

      method inputEdges1  = Set.iter (fun el -> (Graphics.createEdge1 el) ) self#representation.transitions

      method paint state length final alphExists = 
          if alphExists then
            (if (length != 0) then 
              Graphics.paintNode state stepState
            else 
                (if (final) then
                  Graphics.paintNode state acceptState
                else 
                 Graphics.paintNode state wrongFinalState))
          else 
            (Graphics.paintNode state wrongFinalState;
            JS.alert ("There is no transition with the symbol given!"))
            

      method paintStates length states alphExists = 
              Graphics.resetStyle();
              JS.log ("Blah");
              Set.iter (fun el -> self#paint el length (Set.belongs el self#representation.acceptStates) alphExists) states

      method accept3 (w: word) =
          let transition sts sy t = 
            let nsts = Set.flatMap (fun st -> nextStates st sy t) sts in
                      Set.union nsts (nextEpsilons nsts t) in
						
          let rec accept2X sts w t exists =
            match w with
              [] -> Lwt.bind (delay 100) (fun () -> Lwt.bind (Lwt.return (self#paintStates (List.length w) sts exists)) (fun () -> Lwt.return ((Set.inter sts self#representation.acceptStates) <> Set.empty)))
              |x::xs -> Lwt.bind (delay 50) (fun () -> Lwt.bind (Lwt.return (self#paintStates (List.length w) sts exists)) 
                (fun () -> let nextTrans = transition sts x t in
                              if (Set.size nextTrans) = 0 then
                                accept2X sts [] t false
                              else 
                                accept2X (transition sts x t) xs t true
                )) in
						
          let i = closeEmpty (Set.make [self#representation.initialState]) self#representation.transitions in
                  accept2X i w self#representation.transitions true

      method startAccept =
        steps <- Array.make 1000 Set.empty;
        position <- 0;	
        isOver <- false;
        let i = closeEmpty (Set.make [self#representation.initialState]) self#representation.transitions in
          Array.set steps position i;
        self#paintStates ((List.length !sentence) - position) (Array.get steps position) true; 
        if (position = (List.length !sentence)) then
          (isOver <- true);
        self#changeSentence ()
                  
      method next =
        if isOver then
          (JS.alert ("A palavra terminou. Não existem mais estados seguintes."))
        else 
        (position <- position + 1;
        let letter = List.nth !sentence (position-1) in 
        let nextSteps = (transition (Array.get steps (position-1)) letter self#representation.transitions) in
          steps.(position) <- nextSteps;
          if (Set.size nextSteps) = 0 then
            (self#paintStates ((List.length !sentence) - position) (Array.get steps (position-1)) false;
            isOver <- true)
          else
            (self#paintStates ((List.length !sentence) - position) (Array.get steps position) true;
            if (position = (List.length !sentence)) then
              (isOver <- true;)
            );
        self#changeSentence ())

      method back =
        position <- position - 1;
        if position < 0 then
          (position <- 0; JS.alert ("Não é possível andar para trás do estado inicial"))
        else 
          (self#paintStates ((List.length !sentence) - position) (Array.get steps position) (Set.belongs (List.nth !sentence (position)) self#representation.alphabet); self#changeSentence())

      method drawExample = 
        self#inputNodes;
        self#inputEdges

      method drawExample1 = 
        self#inputNodes1;
        self#inputEdges1
      
      method drawMinimize colors number =
        self#inputNodesPainting colors number;
        self#inputEdges1

      method addInitialNode node firstNode exists =
        if firstNode then
          (new FiniteAutomatonAnimation.model (Representation {
            alphabet = Set.empty;
	          allStates = Set.make [node]; 
            initialState = node;
            transitions = Set.empty;
            acceptStates = Set.empty
          }))  
        else
          if exists then
            (let rep: t = self#representation in 
            new FiniteAutomatonAnimation.model (Representation{
              alphabet = rep.alphabet;
	            allStates = rep.allStates;
              initialState = node;
              transitions = rep.transitions;
              acceptStates = rep.acceptStates
            }))
          else
            (let rep: t = self#representation in 
            new FiniteAutomatonAnimation.model (Representation{
              alphabet = rep.alphabet;
	            allStates = Set.add node rep.allStates;
              initialState = node;
              transitions = rep.transitions;
              acceptStates = rep.acceptStates
            }))
      
      method addNode node firstNode =
      if firstNode then
        (new FiniteAutomatonAnimation.model (Representation {
          alphabet = Set.empty;
	        allStates = Set.make [node]; 
          initialState = node;
          transitions = Set.empty;
          acceptStates = Set.empty
        }))  
      else
        (let rep: t = self#representation in 
          new FiniteAutomatonAnimation.model (Representation{
            alphabet = rep.alphabet;
	          allStates = Set.add node rep.allStates;
            initialState = rep.initialState;
            transitions = rep.transitions;
            acceptStates = rep.acceptStates
          }))

      method addFinalNode node firstNode exists = 
        if firstNode then
          (new FiniteAutomatonAnimation.model (Representation {
          alphabet = Set.empty;
	        allStates = Set.make [node]; 
          initialState = node;
          transitions = Set.empty;
          acceptStates = Set.make [node]
        })) 
        else 
          if exists then
          (let rep: t = self#representation in 
            new FiniteAutomatonAnimation.model (Representation{
              alphabet = rep.alphabet;
	            allStates = rep.allStates;
              initialState = rep.initialState;
              transitions = rep.transitions;
              acceptStates = Set.add node rep.acceptStates
            }))
          else 
            (let rep: t = self#representation in 
              new FiniteAutomatonAnimation.model (Representation{
              alphabet = rep.alphabet;
	            allStates = Set.add node rep.allStates;
              initialState = rep.initialState;
              transitions = rep.transitions;
              acceptStates = Set.add node rep.acceptStates
            }))

      method eliminateNode node isStart isFinish = 
        let rep: t = self#representation in 
        if (isStart && isFinish) then
          new FiniteAutomatonAnimation.model (Representation{  
            alphabet = rep.alphabet;
	          allStates = Set.remove node rep.allStates;
            initialState = "";
            transitions = rep.transitions;
            acceptStates = Set.remove node rep.acceptStates
            })
        else
          if (isStart) then
            new FiniteAutomatonAnimation.model (Representation{  
              alphabet = rep.alphabet;
	            allStates = Set.remove node rep.allStates;
              initialState = "";
              transitions = rep.transitions;
              acceptStates = rep.acceptStates
            })
          else 
            if (isFinish) then
              new FiniteAutomatonAnimation.model (Representation{  
                alphabet = rep.alphabet;
	            allStates = Set.remove node rep.allStates;
              initialState = rep.initialState;
              transitions = rep.transitions;
              acceptStates = Set.remove node rep.acceptStates
            })
          else
            new FiniteAutomatonAnimation.model (Representation{  
            alphabet = rep.alphabet;
	          allStates = Set.remove node rep.allStates;
            initialState = rep.initialState;
            transitions = rep.transitions;
            acceptStates = rep.acceptStates
            })


      method newTransition (a, b, c) = 
      let rep: t = self#representation in 
        new FiniteAutomatonAnimation.model (Representation{
            alphabet = Set.add b rep.alphabet;
	          allStates = rep.allStates;
            initialState = rep.initialState;
            transitions = Set.add (a, b , c) rep.transitions;
            acceptStates = rep.acceptStates
      })

      method eliminateTransition (a, b, c) = 
        let rep: t = self#representation in 
        new FiniteAutomatonAnimation.model (Representation{
            alphabet = Set.remove b rep.alphabet;
	          allStates = rep.allStates;
            initialState = rep.initialState;
            transitions = Set.remove (a, b , c) rep.transitions;
            acceptStates = rep.acceptStates
      })

      method getColors:int =
        Set.size self#equivalencePartition

      method paintMinimization colors: unit = 
          let number = self#getColors in
          let listEquivalence = Set.toList self#equivalencePartition in
          for i=0 to number-1 do 
            getMinStates (List.nth listEquivalence i) (Array.get colors i)
          done

      method productivePainting =
        let list1 = Set.toList self#productive in
        iterateList paintProductive list1

      method reachablePainting =
        let list1 = Set.toList (self#reachable (self#representation.initialState)) in
        JS.log (List.length list1);
      iterateList paintReachable list1 

      method usefulPainting =
        let intre = intersection (Set.toList self#productive) (Set.toList (self#reachable (self#representation.initialState))) in
        iterateList paintUseful intre 

      method stringAsList1 s = stringAsList s

      method changeTheTestingSentence word =
        sentence := stringAsList word

      method newSentence1 = !newSentence

      method minimize1: FiniteAutomatonAnimation.model = 			
        let blah = super#minimize in
          let rep = blah#representation in 
            new FiniteAutomatonAnimation.model (Representation rep) 
 
    method toDeterministic1: FiniteAutomatonAnimation.model = 
      let blah = super#toDeterministic in
        let rep = blah#representation in 
        new FiniteAutomatonAnimation.model (Representation rep) 


      method cleanUselessStates1: FiniteAutomatonAnimation.model =
        Graphics.resetStyle();
        let uss = self#getUselessStates in
          Set.iter (fun el -> paintUseful el) uss;
        let blah = super#cleanUselessStates in
        let rep = blah#representation in 
        new FiniteAutomatonAnimation.model (Representation rep) 

      method numberStates: int =
        Set.size self#representation.allStates

      method numberTransitions: int =
        Set.size self#representation.transitions

      method toRegularExpression1 =
        let reg = self#toRegularExpression in
          reg#simplify
      
							
    end

  end

module rec RegularExpressionAnimation : sig
type t = RegExpSyntax.t

type reTree =
    Fail 
    | Tree of word * RegExpSyntax.t * reTree list

	val modelDesignation: unit -> string
	class model :
	  t JSon.alternatives ->
		  object
			method kind : string
			method description : string
			method name : string
			method errors : string list
			method handleErrors : unit
			method validate : unit
			method toJSon: json

			method accept : word -> bool
			method generate : int -> words
			method tracing : unit
			
			method alphabet : symbols
			method quasiLanguage : words 

      method simplify : RegularExpression.model
			method toFiniteAutomaton : FiniteAutomaton.model
			
			method representation : t
			method checkEnumeration : Enumeration.enum -> bool

      method stringAsList1: string -> char list
      method accept1: OCamlFlat.word -> reTree

      method countTree: reTree -> int

      method printTree: reTree -> string

      method checkEnumerationFailures: OCamlFlat.Enumeration.enum -> OCamlFlat.words * OCamlFlat.words

      method getTrees: OCamlFlat.word -> reTree list

      method startAllTrees:  OCamlFlat.word -> unit

      method back: RegularExpressionAnimation.reTree
      method next: RegularExpressionAnimation.reTree

      method length: int 

      method countRightTrees: OCamlFlat.word -> int * int

      method position: int 

      method getRightTrees: reTree

      method getWrongTrees: reTree

		  end
end
 =
struct
	open RegExpSyntax
	type t = RegExpSyntax.t

  type reTree =
    Fail 
    | Tree of word * RegExpSyntax.t * reTree list

  let modelDesignation () = "regular expression"

  let cut s = (String.get s 0, String.sub s 1 ((String.length s)-1)) ;;

  let rec stringAsList s =
        if s = "" then []
        else
            let (x,xs) = cut s in
                x::stringAsList xs
    ;;

  let rec isNotFail t = 
        match t with
          Fail -> false
          | Tree ([], re, []) -> true
          | Tree (w, re, []) -> true
          | Tree ([], re, x::xs) -> (isNotFail x) && (isNotFail (Tree ([], re, xs)))
          | Tree (w, re, x::xs) -> (isNotFail x) && (isNotFail (Tree (w, re, xs)))

  class model (arg: RegularExpression.t JSon.alternatives) =
		object(self) inherit RegularExpression.model arg as super

      val mutable allTrees = [Fail]
      val mutable rightTrees = [Fail]

      val mutable typeOfTree = "wrong"

      val mutable position = 0

      method stringAsList1 = stringAsList

      method countTree t = 
        match t with
          Fail -> 0
          | Tree (w, re, []) -> 1
          | Tree (w, re, x::xs) -> (self#countTree x) + (self#countTree (Tree (w, re, xs)))

      method countRightTrees (w: word): int * int  = 
        let partition w = 
					let rec partX w pword =
						match w with 
							[] -> Set.empty
							| x::xs -> let fwp = pword@[x] in
											Set.add (fwp, xs) (partX xs fwp) in
						 Set.add ([],w) (partX w []) in		
				
				let rec acc w rep = 				
					match rep with 
						| Plus(l, r) -> 
                let (l1, l2) = acc w l in
                let (r1, r2) = acc w r in 
                  (l1 + r1, l2 + r2)
						| Seq(l, r) -> let wps =  partition w in
                      let wpl = Set.toList wps in 
                        List.fold_left (fun (a, b) (c,d) -> (a+c, b+d)) (0,0) (List.map (fun (wp1,wp2) -> 
                        let (sl, fl) = acc wp1 l in
                        let (sr, fr) = acc wp2 r in
                          (sl * sr, fl * fr + fl * sr + fr * sl)
                        ) wpl)							
						| Star(re) -> if w = [] then 
                  (1,0)
                else 						 
									  (let wps = Set.remove ([],w) (partition w) in
                      let wpl = Set.toList wps in 
                       List.fold_left (fun (a, b) (c,d) -> (a+c, b+d)) (0,0) (List.map (fun (wp1,wp2) -> 
                        let (sl, fl) = acc wp1 re in
                        let (sr, fr) = acc wp2 (Star re) in
                          (sl * sr, fl * fr + fl * sr + fr * sl)
                        ) wpl))							
						| Symb(c) -> 
                if w = [c] then 
                  (1,0)
                else 
                  (0,1)
						| Empty -> 
                if w = [] then
                  (1,0) 
                else
                 (0,1)
						| Zero -> (0,1)
				in	
					
					acc w self#representation

      method printTree t =
        match t with 
          Fail -> "Fail" ^ "|/"
          | Tree ([], re, []) -> ""
          | Tree ([], re, x::xs) -> (self#printTree x) ^ "" ^ (self#printTree (Tree ([], re, xs))) ^ ""
          | Tree (w, re, []) -> let blah = String.concat "" (List.map (String.make 1) w) in
                                let regex = RegExpSyntax.toString re in
                                blah ^ "<=>" ^ regex ^ "|/"
          | Tree (w, re, x::xs) -> let regex = RegExpSyntax.toString re in
                                    let blah = String.concat "" (List.map (String.make 1) w) in
                                    blah ^ "<=>" ^ regex ^ "|"  ^ (self#printTree x) ^ "" ^ (self#printTree (Tree ([], re, xs))) ^ "/"

      method accept1 (w: word): reTree  = 
        let partition w = 
					let rec partX w pword =
						match w with 
							[] -> Set.empty
							| x::xs -> let fwp = pword@[x] in
											Set.add (fwp, xs) (partX xs fwp) in
						 Set.add ([],w) (partX w []) in		

        let rec reFind f l =
          match l with 
            [] -> Fail
            | x::xs -> let res = f x in 
                          if res = Fail then
                            reFind f xs
                          else res	in		
				
				let rec acc w rep = 				
					match rep with 
						| Plus(l, r) -> 
                let l1 = acc w l in
                let r1 = acc w r in 
                if l1 = Fail then
                  if r1 = Fail then 
                    Fail
                  else 
                    Tree (w, rep, [r1])
                else 
                  Tree (w, rep, [l1])
						| Seq(l, r) -> let wps =  partition w in
                      let wpl = Set.toList wps in 
                      reFind (fun (wp1,wp2) -> 
                        let tl = acc wp1 l in
                        let tr = acc wp2 r in
                        if tl = Fail || tr = Fail then 
                          Fail
                        else 
                          Tree (w, rep, [tl; tr])
                        ) wpl							
						| Star(re) -> if w = [] then 
                  Tree (w, rep, []) 
                else 						 
									  (let wps = Set.remove ([],w) (partition w) in
                      let wpl = Set.toList wps in 
                      reFind (fun (wp1,wp2) -> 
                        let tl = acc wp1 re in
                        let tr = acc wp2 (Star re) in
                        if tl = Fail || tr = Fail then 
                          Fail
                        else 
                          Tree (w, rep, [tl; tr])
                        ) wpl)
						| Symb(c) -> 
                if w = [c] then 
                  Tree (w, rep, []) 
                else 
                  Fail
						| Empty -> 
                if w = [] then
                  Tree (w, rep, []) 
                else
                 Fail
						| Zero -> Fail
				in	
					
					acc w self#representation     

      method getTrees w = 
      let partition w = 
					let rec partX w pword =
						match w with 
							[] -> Set.empty
							| x::xs -> let fwp = pword@[x] in
											Set.add (fwp, xs) (partX xs fwp) in
						 Set.add ([],w) (partX w []) in		
        
				let rec acc w rep = 				
					match rep with 
						| Plus(l, r) -> 
                let l1 = acc w l in
                let r1 = acc w r in 
                  List.map (fun t -> Tree (w, rep, [t])) (l1 @ r1)
						| Seq(l, r) -> let wps =  partition w in
                      let wpl = Set.toList wps in 
                      List.flatten (List.map (fun (wp1, wp2) ->  
                        let tl = acc wp1 l in
                        let tr = acc wp2 r in
                        List.flatten (List.map (fun x -> List.map (fun y -> Tree (w, rep, [x; y])) tr) tl)    
                      ) wpl)
						| Star(re) -> if w = [] then 
                  [Tree (['~'], rep, [])] 
                else 						 
									  (let wps = Set.remove ([],w) (partition w) in
                      let wpl = Set.toList wps in 
                        List.flatten (List.map (fun (wp1, wp2) ->  
                        let tl = acc wp1 re in
                        let tr = acc wp2 (Star re) in
                        List.flatten (List.map (fun x -> List.map (fun y -> Tree (w, rep, [x; y])) tr) tl)    
                      ) wpl))
						| Symb(c) -> 
                if w = [c] then 
                  [Tree (w, rep, [])]
                else 
                  [Tree (w, rep, [Fail])]
						| Empty -> 
                if w = [] then
                  [Tree (['~'], rep, [])]
                else
                 [Tree (w, rep, [Fail])]
						| Zero -> [Tree (w, rep, [Fail])]
				in	
					
					acc w self#representation

      method startAllTrees w =
        allTrees <- self#getTrees w;
        position <- 0
        
      method next = 
        position <- position + 1;
        if position > (List.length allTrees) -1 then
          position <- 0;
        List.nth allTrees position

      method back = 
        position <- position - 1;
        if position < 0 then
          position <- (List.length allTrees) - 1;
        List.nth allTrees position

      method length =
        List.length allTrees

      method position =
        position +1 

      method getRightTrees =
        let rightTrees = List.filter (fun x -> isNotFail x) allTrees in 
        allTrees <- rightTrees;
        List.nth allTrees position

      method getWrongTrees =
        List.nth allTrees position
      

  	end

end

]
